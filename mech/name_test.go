package mech_test

import (
	"regexp"
	"testing"

	"gitlab.com/fospathi/universal/mech"
)

func TestNewRandHexStr(t *testing.T) {

	// A regexp to match hexadecimal strings with lowercase letters.
	reg := regexp.MustCompile("^[0-9a-f]+$")

	// Test even length strings.

	hexStr32 := mech.NewRandHexStr(32)
	s1, s2 := hexStr32.String(), hexStr32.String()

	{
		want := 32
		got := len(s1)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := len(s1) == len(s2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := s1 != s2
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := reg.MatchString(s1) && reg.MatchString(s2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	// Test odd length strings.

	hexStr1 := mech.NewRandHexStr(1)
	s1, s2 = hexStr1.String(), hexStr1.String()

	{
		want := 1
		got := len(s1)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := len(s1) == len(s2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := s1 != s2
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := reg.MatchString(s1) && reg.MatchString(s2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	hexStr9 := mech.NewRandHexStr(9)
	s1, s2 = hexStr9.String(), hexStr9.String()

	{
		want := 9
		got := len(s1)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := len(s1) == len(s2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := s1 != s2
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := true
		got := reg.MatchString(s1) && reg.MatchString(s2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
