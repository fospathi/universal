package mech

const (
	// The number of samples used when performing multisample anti-aliasing.
	MSAA = 8
)

const (
	// Float32sNonEye is the number of float32s in a quadratic Bezier's vertex
	// in the vertex attributes array, not including the toEye transformation.
	Float32sNonEye = 7

	// Float32sEye is the number of float32s in the toEye transformation: 3 rows
	// x 4 columns (affine) transformation.
	Float32sEye = 12
)

// A Depictioner forms a depiction of (a thing)/(some things) using styled
// quadratic Bezier curves in 3D space.
type Depictioner interface {
	Depiction() Depiction
	Name() string
}

// A Depiction is a number of styled quadratic Bezier curves in 3D space which
// constitute a wireframe depiction of (a thing)/(some things), like for example
// the approximate shape of a curve's graph or the outline of a machine.
type Depiction struct {
	// The quadratic Bezier curves' vertex attributes. Each vertex has the same
	// number and type of attributes as the others and so uses the same number
	// of float32s.
	//
	// Each vertex consists of position, colour, and thickness. After a
	// coalescence into one frame each vertex also has a toEye (modelview)
	// affine transformation.
	//
	// A vertex is shared if it is referenced more than once in the quadratics
	// array.
	VertexAttributes []float32

	// Each value in the array is the index of a vertex in the vertex attributes
	// array and groups of three of these indices define the three control
	// points of a quadratic Bezier curve.
	//
	// Using indices allows sharing of vertex attributes between connected
	// Bezier curves which saves space.
	//
	// The length shall be a multiple of 3, that is, the length shall be 3 times
	// the number of quadratic Bezier curves.
	Quadratics []uint32

	// The indices of quadratics that use a closing line-cap.
	//
	// Each value in the array is the index of a vertex in the vertex attributes
	// array and groups of three of these indices indicate a Bezier curve that
	// should be adorned with a circular line-cap on its right/finishing side.
	//
	// The length shall be a multiple of 3, that is, the length shall be 3 times
	// the number of quadratic Bezier curves that use a closing cap.
	Closing []uint32

	// The indices of quadratics that use an opening line-cap.
	//
	// Each value in the array is the index of a vertex in the vertex attributes
	// array and groups of three of these indices indicate a Bezier curve that
	// should be adorned with a circular line-cap on its left/beginning side.
	//
	// The length shall be a multiple of 3, that is, the length shall be 3 times
	// the number of quadratic Bezier curves that use an opening cap.
	Opening []uint32
}

// Append the argument depiction to the receiver and return the result.
//
// Assumes that this is a version without the toEye transformations added to it
// yet.
func (pic1 Depiction) Append(pic2 Depiction) Depiction {
	n := uint32(len(pic1.VertexAttributes) / Float32sNonEye)
	var i, l int
	for i, l = 0, len(pic2.Quadratics); i < l; i++ {
		pic2.Quadratics[i] = pic2.Quadratics[i] + n
	}
	for i, l = 0, len(pic2.Closing); i < l; i++ {
		pic2.Closing[i] = pic2.Closing[i] + n
	}
	for i, l = 0, len(pic2.Opening); i < l; i++ {
		pic2.Opening[i] = pic2.Opening[i] + n
	}
	pic1.VertexAttributes =
		append(pic1.VertexAttributes, pic2.VertexAttributes...)
	pic1.Quadratics = append(pic1.Quadratics, pic2.Quadratics...)
	pic1.Closing = append(pic1.Closing, pic2.Closing...)
	pic1.Opening = append(pic1.Opening, pic2.Opening...)
	return pic1
}

// Copy the depiction using new slices.
func (pic1 Depiction) Copy() Depiction {
	pic2 := Depiction{
		VertexAttributes: make([]float32, len(pic1.VertexAttributes)),
		Quadratics:       make([]uint32, len(pic1.Quadratics)),
		Closing:          make([]uint32, len(pic1.Closing)),
		Opening:          make([]uint32, len(pic1.Opening)),
	}
	copy(pic2.VertexAttributes, pic1.VertexAttributes)
	copy(pic2.Quadratics, pic1.Quadratics)
	copy(pic2.Closing, pic1.Closing)
	copy(pic2.Opening, pic1.Opening)
	return pic2
}

// Count of the vertices represented in the vertex attributes array assuming
// that the vertex attributes array does not include the toEye transformations.
func (pic1 Depiction) Count() uint32 {
	return uint32(len(pic1.VertexAttributes) / Float32sNonEye)
}

// Equals is true if the depictions are equal in terms of content and order of
// content.
func (pic1 Depiction) Equals(pic2 Depiction) bool {
	if len(pic1.Closing) != len(pic2.Closing) ||
		len(pic1.Opening) != len(pic2.Opening) ||
		len(pic1.Quadratics) != len(pic2.Quadratics) ||
		len(pic1.VertexAttributes) != len(pic2.VertexAttributes) {
		return false
	}
	for i := 0; i < len(pic1.Closing); i++ {
		if pic1.Closing[i] != pic2.Closing[i] {
			return false
		}
	}
	for i := 0; i < len(pic1.Opening); i++ {
		if pic1.Opening[i] != pic2.Opening[i] {
			return false
		}
	}
	for i := 0; i < len(pic1.Quadratics); i++ {
		if pic1.Quadratics[i] != pic2.Quadratics[i] {
			return false
		}
	}
	for i := 0; i < len(pic1.VertexAttributes); i++ {
		if pic1.VertexAttributes[i] != pic2.VertexAttributes[i] {
			return false
		}
	}
	return true
}
