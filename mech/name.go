package mech

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

// RandHexStr is a concurrency safe pseudorandom string generator consisting of
// hexadecimal digits with lowercase letters.
//
// Not suitable for security oriented applications requiring true randomness.
type RandHexStr struct {
	sync.Mutex
	l int
	r *rand.Rand
}

// String of pseudorandom hexadecimal digits with a known fixed length and
// lowercase letters.
//
// The prefix associated with hexadecimal numbers is not included, that is, the
// 0x characters are not used as a prefix.
//
// Not suitable for security oriented applications requiring true randomness.
func (rhs *RandHexStr) String() string {
	rhs.Lock()
	defer rhs.Unlock()

	if rhs.l%2 == 0 {
		b := make([]byte, rhs.l/2)
		rhs.r.Read(b)
		return fmt.Sprintf("%x", b)
	}
	b := make([]byte, (rhs.l+1)/2)
	rhs.r.Read(b)
	return fmt.Sprintf("%x", b)[:rhs.l]
}

// NewRandHexStr constructs a new RandHexStr that generates strings of the given
// length.
func NewRandHexStr(l int) *RandHexStr {
	if l < 1 {
		l = 1
	}
	return &RandHexStr{
		Mutex: sync.Mutex{},
		l:     l,
		r:     rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

// HexStr32 is a generator of strings with a fixed length of 32 and
// consisting of 32 pseudorandom hexadecimal digits with lowercase letters.
//
// Not suitable for security oriented applications requiring true randomness.
var HexStr32 = NewRandHexStr(32)
