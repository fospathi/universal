package mech

import (
	"crypto/rand"
	"encoding/base64"
	"net"
	"time"

	"github.com/gorilla/websocket"
)

const (
	// WireserverHostname is the default hostname of a wiregl wireserver.
	WireserverHostname = "127.0.0.1"
	// WireserverPort is the default port of a wiregl wireserver. A value of
	// zero means use an ephemeral port.
	WireserverPort = uint(0)

	// WireserverTCPGobPath is the path part of a wireserver's URL that accepts
	// tcp requests with gob content.
	WireserverTCPGobPath = "/tcp_gob"
	// WireserverWSBinPath is the path part of a wireserver's URL that accepts
	// websocket connections with binary content.
	WireserverWSBinPath = "/ws_bin"
	// WireserverWSGobPath is the path part of a wireserver's URL that accepts
	// websocket connections with gob content.
	WireserverWSGobPath = "/ws_gob"

	// WireserverMaxRasteriseQLen is the capacity of the queue for wiregl scene
	// rasterise requests.
	WireserverMaxRasteriseQLen = 512
)

const (
	// ServerHostname is the default hostname of a Mechane server.
	ServerHostname = "127.0.0.1"
	// ServerPort is the default port of a Mechane server. A value of zero means
	// use an ephemeral port.
	ServerPort = uint(0)

	// Static data paths.

	// AboutPath is the path part of the Mechane URL which lets you get
	// information about Mechane.
	AboutPath = "/about"

	// ElmMotifPath is the path part of the Mechane URL which lets you get the
	// theme settings for the Elm integrant.
	ElmMotifPath = "/motif/elm"
	// TypescriptMotifPath is the the path part of the Mechane URL which lets
	// you get the theme settings for the Typescript integrant.
	TypescriptMotifPath = "/motif/typescript"

	// WorldWebPath is a path prefix used in the path part of the Mechane URL by
	// static files made available to the UI by the world.
	WorldWebPath = "/worldweb"

	// Dynamic data paths.

	// CameraPath is the path part of the Mechane URL which provides a
	// constantly updated list of available camera names.
	CameraPath = "/camera"
	// CameraAssocReqCap is the maximum number of association requests that
	// can be stored in the association requests queue.
	CameraAssocReqCap = 512

	// DevoirPath is the path part of the Mechane URL which receives user
	// interface commands.
	DevoirPath = "/devoir"
	// DevoirCmdCap is the maximum number of commands that can be stored in the
	// commands queue.
	DevoirCmdCap = 512

	// OverlayerPath is the path part of the Mechane URL which updates
	// overlayers.
	OverlayerPath = "/overlayer"

	// PixelPath is the path part of the Mechane URL which dispenses pixels.
	PixelPath = "/pixel"

	// ShutterbugPath is the path part of the Mechane URL which conducts
	// shutterbug related things.
	ShutterbugPath = "/shutterbug"

	// UIEventPath is the path part of the Mechane URL which receives user
	// interface events.
	UIEventPath = "/uievent"

	// ShutterbugBits is the number of bits in a shutterbug.
	ShutterbugBits = 256
	// ShutterbugSourceBits is the number of bits in a shutterbug UI events
	// input source.
	ShutterbugSourceBits = ShutterbugBits

	// ShutterbugMinCheckTime is the minimum amount of time the record of
	// shutterbugs takes when checking if it contains a shutterbug with a given
	// name.
	ShutterbugMinCheckTime = 100 * time.Microsecond
	// ShutterbugMinCreationTime is the minimum amount of time the record of
	// shutterbugs takes when creating a random shutterbug.
	ShutterbugMinCreationTime = 100 * time.Millisecond
	// ShutterbugTimeout is how long a client has to send a shutterbug before a
	// new request is closed.
	ShutterbugTimeout = 1 * time.Second
)

const (
	// ShutdownPeriodLimit is the maximum duration a Mechane server waits after
	// receiving a shut down command before finally closing the server process.
	ShutdownPeriodLimit = time.Duration(5 * time.Second)

	// ShutdownFromTerminal is the reason given to clients for a Mechane server
	// shutdown when it is caused by an interrupt signal given on the terminal.
	ShutdownFromTerminal = "intercepted terminal interrupt"

	// ShutdownByClient is the reason given to clients for a Mechane server
	// shutdown when it is caused by a request from a client.
	ShutdownByClient = "received client request"
)

// RandBase64String returns a URL safe base64 encoding of a secure random byte
// sequence where the random byte sequence consists of the argument number of
// bytes.
func RandBase64String(n int) (string, error) {
	b := make([]byte, n)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return base64.URLEncoding.EncodeToString(b), nil
}

// NewAddr constructs a new Addr.
func NewAddr(listener net.Listener) Addr {
	return Addr{
		Address: listener.Addr().String(),
		Port:    listener.Addr().(*net.TCPAddr).Port,
		URL:     "http://" + listener.Addr().String(),
	}
}

// Addr is the address information for a site.
type Addr struct {
	Address string
	Port    int
	URL     string
}

// InternalServerError closes a websocket due to an internal server error and
// waits a while.
//
// The optional message cannot exceed 123 characters.
func InternalServerError(c *websocket.Conn, msg string) {
	c.WriteMessage(
		websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.CloseInternalServerErr, msg),
	)
	time.Sleep(WSCloseDelay)
}

// NormalClosure closes a websocket due to a normal closure.
//
// The optional message cannot exceed 123 characters.
func NormalClosure(c *websocket.Conn, msg string) error {
	err := c.WriteMessage(
		websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.CloseNormalClosure, msg),
	)
	time.Sleep(WSCloseDelay)
	return err
}

// PolicyViolation closes a websocket for a policy violation and waits a while.
//
// The optional message cannot exceed 123 characters.
func PolicyViolation(c *websocket.Conn, msg string) {
	c.WriteMessage(
		websocket.CloseMessage,
		websocket.FormatCloseMessage(websocket.ClosePolicyViolation, msg),
	)
	time.Sleep(WSCloseDelay)
}

const (
	// WSCloseDelay is the duration to wait after sending a close message on a
	// websocket.
	WSCloseDelay = 1 * time.Second
)
