package mech

// SmallScreenAlpha is the default opacity applied to the smaller overlapping
// camera views in a stitching op.
const SmallScreenAlpha = 0.6

// A Colour is an sRGB colour space colour with components in the range 0..1
// with each component occupying 32 bits.
type Colour [3]float32

// Clamped is the receiver colour but with the components clamped to the range
// 0..1.
func (rgb Colour) Clamped() Colour {
	for i := 0; i < 3; i++ {
		if rgb[i] < 0 {
			rgb[i] = 0
		} else if rgb[i] > 1 {
			rgb[i] = 1
		}
	}
	return rgb
}

// A Colourer deterministically outputs colours as a function of an input
// parameter.
type Colourer interface {
	// Colour takes a parameter that varies in the range 0..1 and returns a
	// colour.
	Colour(t float64) Colour

	// Colours is a sample of colours from the colourer between the two given
	// colour specifying parameters in the range 0..1.
	Colours(t1, t2 float64) Colourer

	// Uniform reports whether the colourer returns the same colour for all
	// values of the colour parameter.
	Uniform() bool
}
