package mech

// Icon is a Material Design icon name/ligature and stylistic variant.
type Icon struct {
	Ligature string
	Style    IconStyle
}

// An IconStyle is a stylistic variant of the Material Design icon set.
type IconStyle string

// Stylistic variants of the Material Design icon set.
const (
	RegularStyle  = IconStyle("material-icons")
	OutlinedStyle = IconStyle("material-icons-outlined")
	RoundStyle    = IconStyle("material-icons-round")
	SharpStyle    = IconStyle("material-icons-sharp")
	TwoToneStyle  = IconStyle("material-icons-two-tone")
)

// IsIconStyle reports whether the string is a valid CSS class name associated
// with one of the stylistic variants of the Material Design icons set.
func IsIconStyle(style string) bool {
	switch style {
	case string(RegularStyle):
		fallthrough
	case string(OutlinedStyle):
		fallthrough
	case string(RoundStyle):
		fallthrough
	case string(SharpStyle):
		fallthrough
	case string(TwoToneStyle):
		return true
	default:
		return false
	}
}
