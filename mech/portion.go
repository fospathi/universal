package mech

import (
	"math"

	"gitlab.com/fospathi/universal/interval"
)

// A Portion defines a continuous interval/portion on a curve or something that
// can be depicted with quadratic Bezier curves.
type Portion struct {
	Caps        [2]Linecap // Linecap styles on the left and right limits.
	interval.In            // The domain limits.
	Colourer               // Interpolated RGB colour at any point between the limit colours.
	Th          [2]float64 // Thickness at the left and right limits.
	Qual        int        // A hint on how many Bezier curves should be used to Depict the portion. The meaning varies for each curve type.
}

// DashArray splits the portion into alternating dashes and gaps with the given
// interval lengths.
//
// If the number of lengths is odd then the list is repeated to get an even
// number of lengths.
func (p Portion) DashArray(vl ...float64) []Portion {
	if len(vl)%2 != 0 {
		vl = append(vl, vl...)
	}
	var (
		forward = p.In[1] > p.In[0]
		gap     bool
		n       = len(vl)
		t       float64
		vp      []Portion
	)
	if !forward {
		for i, l := range vl {
			vl[i] = -l
		}
	}
loop:
	for {
		for i := 0; i < n; i++ {
			l := vl[i]
			t += l
			gap = (i % 2) != 0
			if gap {
				continue
			}
			in := interval.In{t - l, t}
			part, ok := p.Intersection(in)
			if !ok {
				break loop
			}
			vp = append(vp, part)
		}
	}
	return vp
}

// Dir is the direction of the interval, either +1 or -1: for a non-zero length
// interval positive implies the interval end limit is greater than the start.
func (p Portion) Dir() float64 {
	if math.Signbit(p.In[1] - p.In[0]) {
		return -1.0
	}
	return 1.0
}

// Intersection checks whether the given interval overlaps with the receiver's
// interval and in the affirmative case returns the intersecting part of the
// portion.
//
// Returns false if the intervals do not overlap.
func (p Portion) Intersection(in interval.In) (Portion, bool) {
	in, ok := p.AsOpenIntersection(in)
	if !ok {
		return Portion{}, false
	}
	if in == p.In {
		return p, true
	}
	t1, t2 := p.TAtX(in[0]), p.TAtX(in[1])
	res := Portion{
		Caps:     p.Caps,
		Colourer: p.Colours(t1, t2),
		In:       in,
		Th:       [2]float64{p.thickness64(t1), p.thickness64(t2)},
		Qual:     int(math.Ceil(float64(p.Qual) * (in.AbsLen() / p.AbsLen()))),
	}
	return res, true
}

// Thickness at the given proportion (in the range 0..1) of the interval.
func (p Portion) thickness64(t float64) float64 {
	if t == 1 {
		return p.Th[1]
	}
	return p.Th[0] + t*(p.Th[1]-p.Th[0])
}

// Thickness32 at the given proportion (in the range 0..1) of the interval.
func (p Portion) Thickness32(t float64) float32 {
	if t == 1 {
		return float32(p.Th[1])
	}
	return float32(p.Th[0] + t*(p.Th[1]-p.Th[0]))
}

// A Linecap style on the start or end of a portion.
type Linecap uint8

// Types of linecap style.
const (
	Circle = Linecap(iota)
	Butt
)
