package mech

import "time"

// Hysteresis tolerances for UI events on ShutterbugPixels' overlayer elements
// such as clicks on mechane-button elements.
const (
	// If the duration, since the most recent successful event before the
	// current event, does not exceed this value then the event is disregarded.
	ClogDuration = 600 * time.Millisecond

	// If the duration, since the most recent mouse down event before the
	// current click event, exceeds this value then the click is disregarded.
	ButtonClickLongest = 300 * time.Millisecond

	// If the scalar sum of the magnitude of each individual mouse move after
	// the most recent mouse down event and before the current click exceeds
	// this value then the click is disregarded.
	MouseMoveFarthest = 2.0
)
