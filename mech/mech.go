/*
Package mech is a low level schema package containing data, types, and
rudimentary functions shared between Mechane dependencies.
*/
package mech

import (
	"errors"
	"os"
	"path"
	"runtime"
)

// Command-line flag values.
const (
	// EmbeddedFlag signifies a private embedded rasteriser.
	EmbeddedFlag = "embedded"
)

const (
	// WToHPixelsRatio is the ratio of the number of horizontal pixels to the
	// number of vertical pixels in the rendering of a Mechane scene.
	//
	// Approximately equal to the golden ratio of (1.618...): 1.6 * 900 = 1440.
	WToHPixelsRatio = 1.6

	// Heights and widths shall be wholly divisible by the small divisor for
	// camera stitching for multiple camera modes.
	atto  = 300
	femto = 450
	pico  = 600
	nano  = 750
	micro = 900
	milli = 1050

	// HPixels is the number of vertical pixels in the rendering of a Mechane
	// scene.
	HPixels = nano

	// WPixels is the number of horizontal pixels in the rendering of a Mechane
	// scene.
	WPixels = int(WToHPixelsRatio * HPixels)

	// A full size scene rendering has side lengths this times as big as its
	// smaller scaled down copy.
	smallDivisor = 3

	// SmallHPixels is the number of vertical pixels in the smaller scaled down
	// copy of the rendering of a Mechane scene.
	SmallHPixels = HPixels / smallDivisor

	// SmallWPixels is the number of horizontal pixels in the smaller scaled
	// down copy of the rendering of a Mechane scene.
	SmallWPixels = WPixels / smallDivisor
)

// ChdirToHere sets the working directory as the directory containing the file
// which contains the caller of this function.
func ChdirToHere() error {
	var (
		filename string
		ok       bool
	)
	if _, filename, _, ok = runtime.Caller(1); !ok {
		return errors.New("runtime.Caller: it was not possible to recover the filename")
	}
	return os.Chdir(path.Dir(filename))
}
