module gitlab.com/fospathi/universal

go 1.20

require (
	github.com/gorilla/websocket v1.5.0
	golang.org/x/exp v0.0.0-20230522175609-2e198f4a06a1
)
