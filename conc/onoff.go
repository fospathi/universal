/*
Package conc helps with concurrent tasks.
*/
package conc

import "sync"

// OnOff is a concurrency safe on-off switch.
//
// The zero value is useful. Shall not be copied after first use.
type OnOff struct {
	m sync.Mutex
	b bool
}

// IsOff is whether the on-off switch is off.
func (oo *OnOff) IsOff() bool {
	oo.m.Lock()
	defer oo.m.Unlock()
	return !oo.b
}

// IsOn is whether the on-off switch is on.
func (oo *OnOff) IsOn() bool {
	oo.m.Lock()
	defer oo.m.Unlock()
	return oo.b
}

// SwitchOff the on-ff switch.
func (oo *OnOff) SwitchOff() {
	oo.m.Lock()
	defer oo.m.Unlock()
	oo.b = false
}

// SwitchOn the on-ff switch.
func (oo *OnOff) SwitchOn() {
	oo.m.Lock()
	defer oo.m.Unlock()
	oo.b = true
}
