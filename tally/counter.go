/*
Package tally helps keep count of monotonically increasing things.
*/
package tally

import (
	"strconv"
	"sync"
)

// A Counter is a counter that increments the count on each click. It is safe
// for concurrent use.
//
// Between resets it shall not be clicked more than (-1 + 2^64) times.
//
// A Counter shall not be copied after first use.
type Counter struct {
	count uint64
	m     sync.Mutex
}

// Click increments the tally counter by one.
func (c *Counter) Click() {
	c.m.Lock()
	defer c.m.Unlock()

	c.count++
}

// Count is the number of clicks since the last reset.
func (c *Counter) Count() uint64 {
	c.m.Lock()
	defer c.m.Unlock()

	return c.count
}

// CountToa is the number of clicks since the last reset as a decimal string.
func (c *Counter) CountToa() string {
	c.m.Lock()
	defer c.m.Unlock()

	const base = 10
	return strconv.FormatUint(c.count, base)
}

// Next gets the current count as a decimal string and then increments the
// counter.
//
// A possible use for this is as a basic unique name generator which when called
// repeatedly generates a sequence of unique numerical names.
func (c *Counter) Next() string {
	return c.PreClickToa()
}

// PreClick increments the tally counter by one and returns the pre-increment
// count.
func (c *Counter) PreClick() uint64 {
	c.m.Lock()
	defer c.m.Unlock()

	c.count++
	return c.count - 1
}

// PreClickToa increments the tally counter by one and returns the pre-increment
// count as a decimal string.
func (c *Counter) PreClickToa() string {
	c.m.Lock()
	defer c.m.Unlock()

	const base = 10
	c.count++
	return strconv.FormatUint(c.count-1, base)
}

// PostClick increments the tally counter by one and returns the new count after
// the increment.
func (c *Counter) PostClick() uint64 {
	c.m.Lock()
	defer c.m.Unlock()

	c.count++
	return c.count
}

// PostClickToa increments the tally counter by one and returns the new count
// after the increment as a decimal string.
func (c *Counter) PostClickToa() string {
	c.m.Lock()
	defer c.m.Unlock()

	const base = 10
	c.count++
	return strconv.FormatUint(c.count, base)
}

// Reset the tally count to zero.
func (c *Counter) Reset() {
	c.m.Lock()
	defer c.m.Unlock()

	c.count = 0
}

// String is the number of clicks since the last reset as a decimal string.
func (c *Counter) String() {
	c.m.Lock()
	defer c.m.Unlock()

	const base = 10
	strconv.FormatUint(c.count, base)
}
