/*
An example of how to use a pubsub to serve a sequence of values to clients.
*/
package main

import (
	"fmt"
	"log"
	"net/url"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/fospathi/universal/watchable"
)

const addr = "localhost:8080"
const path = "/"

// mystring implements the watchable.Marshal interface.
type mystring struct {
	s string
}

func (ms mystring) Marshal() ([]byte, error) {
	return []byte(ms.s), nil
}

func main() {
	done := make(chan struct{})
	time.AfterFunc(2*time.Second, func() {
		close(done)
	})

	ps := watchable.NewPubsub()
	ps.Pub(mystring{"abc"})
	ps.Pub("def") // Server ignores this value as it doesn't implement the Marshal interface.

	cancel, err := ps.ListenAndServeOnWebsocket(watchable.ServeSpec{
		Addr:    addr,
		Pattern: path,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer cancel()

	// Connect to the server and print the received values.
	connect(func(msg []byte) {
		fmt.Printf("%v\n", string(msg))
	})

	time.Sleep(1 * time.Second)
	ps.Pub(mystring{"xyz"})
	ps.Complete()

	<-done
}

// Connect to the pubsub's web server and process each published marshaler value
// with the argument callback function.
func connect(onMsg func(msg []byte)) {
	u := url.URL{Scheme: "ws", Host: addr, Path: path}
	conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial: ", err)
	}

	go func() {
		for {
			code, msg, err := conn.ReadMessage()
			if err != nil {
				if e, ok := err.(*websocket.CloseError); ok {
					if e.Code == websocket.CloseNormalClosure {
						return
					}
				}
				log.Println("read: ", code, err)
				return
			}
			onMsg(msg)
		}
	}()
}
