# A simple pubsub server

This example shows how a pubsub's web server can serve the pubsub's published values to clients over a websocket connection.

To run the example from a terminal, with the current working directory as this directory, execute the command: 
```sh
go run .
```

You should see the output:
```sh
abc
xyz
```
