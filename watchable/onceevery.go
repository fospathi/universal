package watchable

import (
	"sync"
)

// OnceEvery lets you watch for the completion of each first step in a cycle of
// steps which repeats.
//
// For a given onceevery each cycle consists of the same number of steps
// determined during construction.
type OnceEvery struct {
	c    chan struct{}
	oldC chan struct{}
	i    uint
	n    uint
	m    sync.Mutex
}

// Do completes a step and if that step was the first step in the cycle of steps
// executes the argument function in a separate goroutine and does not wait for
// it to finish.
//
// The returned channel allows you to know when the argument function call
// completes and is closed at that point. It is already closed if the argument
// function is not executed at all.
func (oe *OnceEvery) Do(f func()) <-chan struct{} {
	oe.m.Lock()
	defer oe.m.Unlock()

	done := make(chan struct{})
	if oe.step() {
		go func() {
			f()
			close(done)
		}()
	} else {
		close(done)
	}
	return done
}

// ReWatch returns the watch channel for the current cycle of steps and the
// status of the argument watch channel.
//
// The returned watch channel becomes a closed channel at the moment when the
// next first step in the cycle of steps is completed.
//
// Returns true if the argument watch channel is either the current non-closed
// watch channel or the previous cycles's now closed watch channel. When the
// argument is nil returns true if no steps have ever been completed otherwise
// returns false.
//
// If at least one complete cycle of steps has been missed since the argument
// watcher was the watch channel the returned bool value is false.
func (oe *OnceEvery) ReWatch(prev <-chan struct{}) (<-chan struct{}, bool) {
	oe.m.Lock()
	defer oe.m.Unlock()

	if prev == oe.oldC || prev == oe.c {
		return oe.c, true
	}
	return oe.c, false
}

// Does a step and returns true if it was the first step of the cycle.
func (oe *OnceEvery) step() bool {
	var first bool
	if oe.i%oe.n == 0 {
		first = true
		oe.i = 0
		close(oe.c)
		oe.oldC = oe.c
		oe.c = make(chan struct{})
	}
	oe.i++
	return first
}

// Step completes one step.
//
// Should this step be the first step of a new cycle of steps the watch channel
// is closed.
func (oe *OnceEvery) Step() {
	oe.m.Lock()
	defer oe.m.Unlock()

	oe.step()
}

// SyncDo completes a step and if that step was the first step in the cycle of
// steps executes the argument function synchronously.
//
// The argument function should not call any methods on the receiver onceevery
// or a deadlock will occur.
//
// SyncDo is more suitable for a synchronous workflow. If the argument function
// is called then for its duration this method call blocks and any method calls
// on the receiver onceevery in other goroutines will block.
func (oe *OnceEvery) SyncDo(f func()) {
	oe.m.Lock()
	defer oe.m.Unlock()

	if oe.step() {
		f()
	}
}

// Watch returns a watch channel which becomes a closed channel at the moment
// when the next first step in the cycle of steps is completed.
func (oe *OnceEvery) Watch() <-chan struct{} {
	oe.m.Lock()
	defer oe.m.Unlock()

	return oe.c
}

// NewOnceEvery constructs a new OnceEvery with the argument non-zero number of
// steps in each cycle of steps.
//
// The argument number of steps is clamped to the range 1..(largest uint value).
func NewOnceEvery(n uint) *OnceEvery {
	if n == 0 {
		n = 1
	}
	return &OnceEvery{
		c: make(chan struct{}),
		n: n,
	}
}
