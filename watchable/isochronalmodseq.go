package watchable

import (
	"sync"
	"time"
)

// IsochronalModSeq buffers any added values and then rate limits their addition
// to an underlying modulus sequence.
//
// Let t0 be the time at which the isochronal modulus sequence is constructed
// and d be a fixed duration; an available buffered value is added to the
// wrapped sequence at times t0+1*d, t0+2*d, t0+3*d, t0+4*d, ...
type IsochronalModSeq struct {
	// The wrapped modulus sequence.
	ModSeq *ModSeq

	done chan struct{}
	m    sync.Mutex
	q    []interface{}
}

// Add a value to the isochronal modulo sequence's queue.
//
// The value is eventually added to the underlying modulus sequence at a time
// which is some whole number multiple of the sequence's implicit duration after
// the sequence's construction.
func (ims *IsochronalModSeq) Add(v interface{}) {
	ims.m.Lock()
	defer ims.m.Unlock()

	select {
	case <-ims.done:
		return
	default:
	}
	ims.q = append(ims.q, v)
}

// Close closes the isochronal modulo sequence and all watch channels.
//
// No more values can be added afterwards.
func (ims *IsochronalModSeq) Close() {
	ims.m.Lock()
	defer ims.m.Unlock()

	select {
	case <-ims.done:
		return
	default:
	}
	close(ims.done)
	ims.ModSeq.Close()
}

// NewIsochronalModSeq constructs a new IsochronalModSeq.
//
// The microseconds argument specifies, under ideal circumstances, the minimum
// time interval between additions to the underlying modulus sequence. The on
// empty function is called if the iso's buffer is empty when an addition to the
// underlying sequence comes due.
//
// The on empty function can be nil. Otherwise, ideally, it should be a fast
// function that completes in less than a microsecond to maintain the iso's
// isochronal property. The on empty function is called outside the iso's mutex
// so its okay to call the iso's methods in the function but this means the
// state can be changed by other goroutines during the function's lifetime.
//
// The microseconds duration and sequence modulus arguments are clamped to the
// range [1, largest uint value].
func NewIsochronalModSeq(
	resolution Microseconds,
	modulus uint,
	onEmpty func(*IsochronalModSeq),
) *IsochronalModSeq {
	if modulus == 0 {
		modulus = 1
	}
	if resolution == 0 {
		resolution = 1
	}
	ims := &IsochronalModSeq{}
	ims.ModSeq = NewModSeq(modulus)
	ims.done = make(chan struct{})
	t := time.NewTicker(time.Duration(resolution) * time.Microsecond)
	onDone := func() {
		t.Stop()
		ims.ModSeq.Close()
	}
	go func() {
	loop:
		for {
			select {
			case <-t.C:
				select {
				case <-ims.done:
					onDone()
					break loop
				default:
				}
				ims.m.Lock()
				if len(ims.q) == 0 {
					if nil == onEmpty {
						ims.m.Unlock()
						continue
					}
					ims.m.Unlock()
					// Call the on empty function outside the lock to prevent
					// deadlocks if it calls ims methods.
					onEmpty(ims)
					ims.m.Lock()
				}
				if len(ims.q) > 0 {
					ims.ModSeq.Add(ims.q[0])
					ims.q = ims.q[:copy(ims.q[:], ims.q[1:])]
				}
				ims.m.Unlock()
			case <-ims.done:
				onDone()
				break loop
			}
		}
	}()
	return ims
}
