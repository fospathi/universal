package watchable_test

import (
	"math"
	"testing"
	"time"

	"gitlab.com/fospathi/universal/watchable"
)

func TestChronoDial_AtPositionFor(t *testing.T) {
	var delay uint = 10000
	var halfDelay uint = delay / 2

	cd := watchable.NewChronoDial(watchable.ChronoDialSpec{
		Dozens:     watchable.Dozens(1),
		Resolution: watchable.Microseconds(delay),
		Immediate:  true,
	})

	time.Sleep(time.Duration(halfDelay) * time.Microsecond)
	v1, err1 := cd.AtPositionFor(0, nil)
	v2, err2 := cd.AtPositionFor(1, nil)
	v3, err3 := cd.AtPositionFor(2, nil)

	{
		want1, want2, want3 := watchable.ChronoShift{}, watchable.ChronoShift{}, watchable.ChronoShift{}
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := watchable.IsWatcherNotFoundError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	time.Sleep(time.Duration(delay) * time.Microsecond)
	v1, err1 = cd.AtPositionFor(0, nil)
	v2, err2 = cd.AtPositionFor(1, nil)
	v3, err3 = cd.AtPositionFor(2, nil)

	{
		want1, want2, want3 := watchable.ChronoShift{}, watchable.ChronoShift{}, watchable.ChronoShift{}
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := watchable.IsWatcherNotFoundError(err1),
			watchable.IsWatcherNotFoundError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	time.Sleep(time.Duration(delay) * time.Microsecond)
	v1, err1 = cd.AtPositionFor(0, nil)
	v2, err2 = cd.AtPositionFor(1, nil)
	v3, err3 = cd.AtPositionFor(2, nil)

	{
		want1, want2, want3 := watchable.ChronoShift{}, watchable.ChronoShift{}, watchable.ChronoShift{}
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := watchable.IsWatcherNotFoundError(err1),
			watchable.IsWatcherNotFoundError(err2),
			watchable.IsWatcherNotFoundError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	cd.Close()
	delay = 6000
	halfDelay = delay / 2
	tolerance := watchable.Microseconds(1000)
	cd = watchable.NewChronoDial(watchable.ChronoDialSpec{
		Dozens:     watchable.Dozens(1),
		Resolution: watchable.Microseconds(delay),
	})

	w1, ok1 := cd.ReWatch(0, nil)
	w2, ok2 := cd.ReWatch(1, nil)
	w3, ok3 := cd.ReWatch(2, nil)
	time.Sleep(time.Duration(delay+halfDelay) * time.Microsecond)
	<-w1
	time.Sleep(time.Duration(delay) * time.Microsecond)
	<-w2
	v1, err1 = cd.AtPositionFor(0, w1)
	v2, err2 = cd.AtPositionFor(1, w2)
	v3, err3 = cd.AtPositionFor(2, w3)

	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := ok1, ok2, ok3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 :=
			err1 == nil,
			err2 == nil,
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		cs1 := watchable.ChronoShift{}
		cs2 := watchable.ChronoShift{MeasuredMonotonic: time.Duration(delay) * time.Microsecond}
		want1, want2, want3 :=
			CompareChronoShifts(v1, cs1, tolerance),
			CompareChronoShifts(v2, cs2, tolerance),
			watchable.ChronoShift{}
		got1, got2, got3 := true, true, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	w10, ok10 := cd.ReWatch(10, nil)
	w11, ok11 := cd.ReWatch(11, nil)
	w0, ok0 := cd.ReWatch(0, w1)
	time.Sleep(time.Duration(delay) * 11 * time.Microsecond)
	<-w10
	<-w11
	<-w0
	v10, err10 := cd.AtPositionFor(10, w10)
	v11, err11 := cd.AtPositionFor(11, w11)
	v0, err0 := cd.AtPositionFor(0, w0)

	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := ok10, ok11, ok0
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := error(nil), error(nil), error(nil)
		got1, got2, got3 := err10, err11, err0
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		cs10 := watchable.ChronoShift{MeasuredMonotonic: time.Duration(delay) * 10 * time.Microsecond}
		cs11 := watchable.ChronoShift{MeasuredMonotonic: time.Duration(delay) * 11 * time.Microsecond}
		cs0 := watchable.ChronoShift{MeasuredMonotonic: time.Duration(delay) * 12 * time.Microsecond}
		want1, want2, want3 := true, true, true
		got1, got2, got3 := CompareChronoShifts(v10, cs10, tolerance),
			CompareChronoShifts(v11, cs11, tolerance),
			CompareChronoShifts(v0, cs0, tolerance)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
}

// Wether the timings of the argument chrono shifts are similar to within the given tolerance.
func CompareChronoShifts(cs1 watchable.ChronoShift, cs2 watchable.ChronoShift,
	tolerance watchable.Microseconds) bool {
	tol := float64(tolerance) * 1e-6
	t1 := cs1.T()
	t2 := cs2.T()
	return math.Abs(t1-t2) <= tol
}

func TestChronoDial_WatchOclock(t *testing.T) {
	cd := watchable.NewChronoDial(watchable.ChronoDialSpec{
		Dozens:     watchable.Dozens(2),
		Resolution: watchable.Microseconds(500),
	})

	w1 := cd.Watch(12)
	w2 := cd.WatchOclock(6)

	{
		want := w1
		got := w2
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	<-w1
	<-w2
	{
		want, _ := cd.AtPosition(12)
		got, _ := cd.AtOclock(6)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
	{
		want, _ := cd.AtPositionFor(12, w1)
		got, _ := cd.AtOclockFor(6, w2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	w1, ok1 := cd.ReWatch(12, w1)
	w2, ok2 := cd.ReWatchOclock(6, w2)
	<-w1
	<-w2

	{
		want1, want2 := true, true
		got1, got2 := ok1, ok2
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}
	{
		want1, want2 := cd.AtPositionFor(12, w1)
		got1, got2 := cd.AtOclockFor(6, w2)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		want := error(nil)
		_, got := cd.AtOclockFor(6, w2)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
