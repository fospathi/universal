package watchable

import (
	"errors"
	"sync"
)

// A Sequence is a concurrency safe watchable sequence of values.
type Sequence struct {
	// The Done channel is closed by the Close() method.
	Done <-chan struct{}
	done chan struct{}

	vv []interface{}
	vw []chan struct{}
	m  sync.Mutex
}

// Add a value to the sequence.
//
// In the sequence, the first value added occupies the first position, the
// second value added occupies the second position and so on.
func (seq *Sequence) Add(v interface{}) {
	seq.m.Lock()
	defer seq.m.Unlock()

	select {
	case <-seq.done:
		return
	default:
	}
	seq.vv = append(seq.vv, v)
	seq.vw = append(seq.vw, make(chan struct{}))
	close(seq.vw[len(seq.vw)-2])
}

// AddAll the argument values to the sequence.
//
// In the sequence, the first value added occupies the first position, the
// second value added occupies the second position and so on.
func (seq *Sequence) AddAll(vv []interface{}) {
	seq.m.Lock()
	defer seq.m.Unlock()

	select {
	case <-seq.done:
		return
	default:
	}
	for _, v := range vv {
		seq.vv = append(seq.vv, v)
		seq.vw = append(seq.vw, make(chan struct{}))
		close(seq.vw[len(seq.vw)-2])
	}
}

// Close closes the sequence.
//
// No more values can be added afterwards.
func (seq *Sequence) Close() {
	seq.m.Lock()
	defer seq.m.Unlock()

	select {
	case <-seq.done:
		return
	default:
	}

	// Close done before closing the last unfilled watch channel to accommodate
	// the end user usage pattern of:
	//
	//     select {
	//     case <-watchChannel:
	//         select {
	//         case <-sequence.Done:
	//             ...
	//             return
	//         default:
	//         }
	//         ...
	//     }
	//
	// Closing the watch channel first could mean done is not closed soon enough
	// to select the done case.
	close(seq.done)
	close(seq.vw[len(seq.vw)-1])
}

// Length of the sequence.
func (seq *Sequence) Length() int {
	seq.m.Lock()
	defer seq.m.Unlock()

	return len(seq.vv)
}

// RemoveFor the argument watch channel's position the value in the sequence
// along with the watcher itself.
//
// Returns an error if no such watch channel exists or the position is not
// filled.
func (seq *Sequence) RemoveFor(watcher <-chan struct{}) (interface{}, error) {
	seq.m.Lock()
	defer seq.m.Unlock()

	l := uint(len(seq.vw))
	if watcher == seq.vw[l-1] {
		return nil, errNonRemaining
	}
	removeAt := func(i uint) interface{} {
		v := seq.vv[i]
		seq.vv = seq.vv[:int(i)+copy(seq.vv[i:], seq.vv[i+1:])]
		seq.vw = seq.vw[:int(i)+copy(seq.vw[i:], seq.vw[i+1:])]
		return v
	}
	for i := uint(0); i < l-1; i++ {
		if watcher == seq.vw[i] {
			return removeAt(i), nil
		}
	}
	return nil, errWatcherNotFound
}

// ValueOf returns the value in the sequence at the argument watch channel's
// position.
//
// Returns an error if no such watch channel exists or the position is not
// filled.
func (seq *Sequence) ValueOf(watcher <-chan struct{}) (interface{}, error) {
	seq.m.Lock()
	defer seq.m.Unlock()

	l := len(seq.vw)
	if watcher == seq.vw[l-1] {
		return nil, errNonRemaining
	}
	for i := l - 2; i >= 0; i-- {
		if watcher == seq.vw[i] {
			return seq.vv[i], nil
		}
	}
	return nil, errWatcherNotFound
}

// Values returns a new slice containing the values added to the sequence thus far.
func (seq *Sequence) Values() []interface{} {
	seq.m.Lock()
	defer seq.m.Unlock()

	result := make([]interface{}, len(seq.vv))
	copy(result, seq.vv)
	return result
}

// Watch the next position in the sequence which comes after the argument closed
// watcher channel's position.
//
// Supply a nil argument to watch for the first value added to the sequence.
// Otherwise, to watch a position, you need to supply the already closed watch
// channel of the preceding position.
//
// The returned watch channel becomes a closed channel upon a value having been
// placed at the position it watches in the sequence. Indeed, the returned
// channel is already a closed channel if a value has already been placed at
// that position.
func (seq *Sequence) Watch(prev <-chan struct{}) (<-chan struct{}, error) {
	seq.m.Lock()
	defer seq.m.Unlock()

	if prev == nil {
		return seq.vw[0], nil
	}
	l := len(seq.vw)
	if prev == seq.vw[l-1] {
		return nil, errNonRemaining
	}
	for i := l - 2; i >= 0; i-- {
		if prev == seq.vw[i] {
			return seq.vw[i+1], nil
		}
	}
	return nil, errWatcherNotFound
}

// NewSequence constructs a new Sequence.
func NewSequence() *Sequence {
	done := make(chan struct{})
	seq := &Sequence{
		done: done,
		Done: done,
		vw:   []chan struct{}{make(chan struct{})},
	}
	return seq
}

// IsNonRemainingError is whether the argument error indicates that a watch
// channel which was supplied as an argument to a sequence's method was already
// the last watchable position and that no more values have been added to the
// sequence yet.
//
// Wait until a watch channel is closed before watching the next position or
// getting its sequence value.
func IsNonRemainingError(err error) bool {
	return err == errNonRemaining
}

var errNonRemaining error = errors.New("no more values: wait until the watch channel becomes closed")

// IsWatcherNotFoundError is whether the argument error indicates that a watch
// channel which was supplied as an argument was not found among the current
// watch channels.
func IsWatcherNotFoundError(err error) bool {
	return err == errWatcherNotFound
}

var errWatcherNotFound error = errors.New("no such watch channel was found")
