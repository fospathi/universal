package watchable_test

import (
	"sync"
	"testing"
	"time"

	"gitlab.com/fospathi/universal/watchable"
)

func TestOnceEvery_Do(t *testing.T) {
	var i int
	inc := func() {
		i++
	}
	oncePer3 := watchable.NewOnceEvery(3)
	doneA := oncePer3.Do(inc)
	<-doneA

	want := 1
	got := i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	doneA = oncePer3.Do(inc)
	doneB := oncePer3.Do(inc)

	<-doneA
	<-doneB

	want = 1
	got = i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	doneA = oncePer3.Do(inc)
	<-doneA

	want = 2
	got = i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestOnceEvery_ReWatch(t *testing.T) {
	oncePer1 := watchable.NewOnceEvery(1)
	iM := sync.Mutex{}
	var i int
	done := make(chan struct{})

	{
		watcher, ok := oncePer1.ReWatch(nil)
		want := true
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		go func() {
			for {
				select {
				case <-watcher:
					iM.Lock()
					i++
					iM.Unlock()
					watcher, ok = oncePer1.ReWatch(watcher)
					got = ok
					if want != got {
						t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
					}
				case <-done:
					return
				}
			}
		}()
	}

	time.Sleep(20 * time.Millisecond)
	want := 0
	iM.Lock()
	got := i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer1.Step()
	time.Sleep(20 * time.Millisecond)
	want = 1
	iM.Lock()
	got = i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer1.Step()
	time.Sleep(20 * time.Millisecond)
	want = 2
	iM.Lock()
	got = i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer1.Step()
	time.Sleep(20 * time.Millisecond)
	want = 3
	iM.Lock()
	got = i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	close(done)

	{
		_, ok := oncePer1.ReWatch(nil)
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

}

func TestOnceEvery_Step(t *testing.T) {
	oncePer2 := watchable.NewOnceEvery(2)
	iM := sync.Mutex{}
	var i int
	done := make(chan struct{})

	{
		watcher := oncePer2.Watch()
		go func() {
			for {
				select {
				case <-watcher:
					iM.Lock()
					i++
					iM.Unlock()
					watcher = oncePer2.Watch()
				case <-done:
					return
				}
			}
		}()
	}

	time.Sleep(20 * time.Millisecond)
	want := 0
	iM.Lock()
	got := i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer2.Step()
	time.Sleep(20 * time.Millisecond)
	want = 1
	iM.Lock()
	got = i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer2.Step()
	time.Sleep(20 * time.Millisecond)
	want = 1
	iM.Lock()
	got = i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer2.Step()
	time.Sleep(20 * time.Millisecond)
	want = 2
	iM.Lock()
	got = i
	iM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	close(done)
}

func TestOnceEvery_SyncDo(t *testing.T) {
	var i int
	inc := func() {
		i++
	}
	oncePer3 := watchable.NewOnceEvery(3)
	oncePer3.SyncDo(inc)

	want := 1
	got := i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer3.SyncDo(inc)
	oncePer3.SyncDo(inc)

	want = 1
	got = i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer3.SyncDo(inc)
	want = 2
	got = i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer3.SyncDo(inc)
	oncePer3.SyncDo(inc)

	want = 2
	got = i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	oncePer3.SyncDo(inc)
	want = 3
	got = i
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
