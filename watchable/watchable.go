/*
Package watchable provides primitives which can signal some kinds of changes to
their state.
*/
package watchable
