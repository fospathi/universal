package watchable

import "sync"

// ModPubsub (modulus Pubsub) is a concurrency safe publish-subscribe like
// pattern over a fixed length sequence of values that represent the tail of an
// unlimited sequence of values.
type ModPubsub struct {
	ModSeq *ModSeq

	done chan struct{}
	s    map[chan interface{}]*Sequence
	wg   sync.WaitGroup
}

// Close the modulus pubsub and its underlying modulus sequence.
//
// Eventually all currently existing subscription channels will be closed. Some
// channels which are still consuming available values may, on a random basis,
// consume more values before closing. Close waits until the subscriptions are
// closed before returning.
func (mps *ModPubsub) Close() {
	mps.ModSeq.m.Lock()
	defer mps.ModSeq.m.Unlock()
	mps.ModSeq.close()

	select {
	case <-mps.done:
		return
	default:
	}
	close(mps.done)
	mps.wg.Wait()
}

// Complete indicates that no more new values will be published.
//
// Completing means new and existing subscription channels will close once they
// have consumed all the published values. Each subscription channel has a
// goroutine which, if not cancelled, won't exit until completion or closure.
//
// New subscriptions to a completed modulus pubsub are still allowed and they
// yield all currently stored values.
func (mps *ModPubsub) Complete() {
	mps.ModSeq.Close()
}

// Pub publishes a value on the pubsub.
//
// Subscribers consume the published values in a first published first consumed
// order.
func (mps *ModPubsub) Pub(v interface{}) {
	mps.ModSeq.Add(v)
}

// Sub returns a subscription channel which receives the currently stored values
// and any future published values.
//
// Cancel the subscription channel prematurely by calling the returned cancel
// function. The subscription channel may, on a random basis, consume more
// values before closing. The cancel function waits until the subscription
// cancellation is accomplished before returning.
//
// If the modulus pubsub has already been closed an error is returned and the
// other return values are nil.
func (mps *ModPubsub) Sub() (<-chan interface{}, CancelFunc, error) {
	mps.ModSeq.m.Lock()
	defer mps.ModSeq.m.Unlock()

	select {
	case <-mps.done:
		return nil, nil, errPubsubClosed
	default:
	}

	cancelC := make(chan struct{})
	cancelM := &sync.Mutex{}
	cancelWG := &sync.WaitGroup{}
	cancel := func() {
		cancelM.Lock()
		defer cancelM.Unlock()
		select {
		case <-cancelC:
			return
		default:
		}
		close(cancelC)
		cancelWG.Wait()
	}

	c := make(chan interface{})
	s := NewSequence()
	s.AddAll(mps.ModSeq.values())
	mps.s[c] = s
	remove := make(chan struct{})
	go func() {
		<-remove
		mps.ModSeq.m.Lock()
		defer mps.ModSeq.m.Unlock()
		delete(mps.s, c)
	}()
	w, _ := s.Watch(nil)
	completed := mps.ModSeq.Done

	mps.wg.Add(1)
	cancelWG.Add(1)
	go func() {
		defer mps.wg.Add(-1)
		defer cancelWG.Add(-1)
		defer close(c)
		defer close(remove)
		for {
			select {
			case <-cancelC:
				return

			case <-mps.done:
				return

			case <-completed:
				s.Close()
				completed = nil

			case <-w:
				select {
				case <-cancelC:
					return
				default:
				}
				v, err := s.ValueOf(w)
				if IsNonRemainingError(err) {
					return
				}
				select {
				case <-cancelC:
					return
				case c <- v:
				}
				prev := w
				w, _ = s.Watch(w)
				s.RemoveFor(prev) // Sent values are no longer required.
			}
		}
	}()

	return c, cancel, nil
}

// NewModPubsub constructs a new ModPubsub with the argument modulus.
func NewModPubsub(mod uint) *ModPubsub {
	if mod == 0 {
		mod = 1
	}
	mps := &ModPubsub{
		ModSeq: NewModSeq(mod),

		done: make(chan struct{}),
		s:    map[chan interface{}]*Sequence{},
		wg:   sync.WaitGroup{},
	}
	mps.ModSeq.onAdd = func(v interface{}) {
		for _, s := range mps.s {
			s.Add(v)
		}
	}
	return mps
}
