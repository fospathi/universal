package watchable

import (
	"errors"
	"sync"
)

// ModSeq (modulo sequence) is a fixed length sequence whose write position is
// reset to the start position everytime the end write position is written to.
//
// Just like an analog clock the modulus sequence uses modular arithmetic. A
// dial position on the clock is a write position and the clock hand is the
// current write position.
//
// For methods which take a position argument the positions can be greater than
// the modulo sequence's length and they only need to be congruent to the
// position modulo the modulus.
type ModSeq struct {
	// The Done channel is closed by the Close() method.
	Done <-chan struct{}
	done chan struct{}

	// Called during Add() before the value is added. Not called after a
	// sequence has been closed. Shall not call methods on this. Shall be fast.
	onAdd func(interface{})

	i   uint
	n   uint // Count of values added.
	m   sync.Mutex
	mod uint // Modulus of modulus sequence.
	vv  []interface{}
	vw  []chan struct{} // The current unclosed watch channels for new values.
	vcw []chan struct{} // The current closed watch channels for the stored values.
}

// Add a value to the modulo sequence at position (n % modulus), where n is
// congruent to the total number of previously added values.
func (ms *ModSeq) Add(v interface{}) {
	ms.m.Lock()
	defer ms.m.Unlock()

	select {
	case <-ms.done:
		return
	default:
	}
	if nil != ms.onAdd {
		ms.onAdd(v)
	}
	c := ms.vw[ms.i]
	ms.vv[ms.i] = v
	ms.vw[ms.i] = make(chan struct{})
	ms.vcw[ms.i] = c
	ms.i++
	ms.i %= ms.mod
	ms.n++
	close(c)
}

func (ms *ModSeq) close() {
	select {
	case <-ms.done:
		return
	default:
	}

	// Close done before closing the last unfilled watch channel to accommodate
	// the end user usage pattern of:
	//
	//     select {
	//     case <-watchChannel:
	//         select {
	//         case <-sequence.Done:
	//             ...
	//             return
	//         default:
	//         }
	//         ...
	//     }
	//
	// Closing the watch channel first could mean done is not closed soon enough
	// to select the done case.
	close(ms.done)
	for _, c := range ms.vw {
		close(c)
	}
}

// Close closes the modulo sequence and all watch channels.
//
// No more values can be added afterwards.
func (ms *ModSeq) Close() {
	ms.m.Lock()
	defer ms.m.Unlock()

	ms.close()
}

// Position of the "clock hand", that is, the current write position in the
// sequence.
func (ms *ModSeq) Position() uint {
	ms.m.Lock()
	defer ms.m.Unlock()

	return ms.i
}

// ReWatchAt the argument position and check whether the argument watcher is
// current.
//
// The returned watch channel becomes a closed channel the next time a value is
// placed at the position it watches or when the modulus sequence is closed.
//
// Return false if values may have been missed, that is, if the argument watch
// channel is neither the preceding nor the active watch channel at this
// position.
//
// Always returns true if the argument watch channel is nil.
func (ms *ModSeq) ReWatchAt(i uint, watcher <-chan struct{}) (<-chan struct{}, bool) {
	ms.m.Lock()
	defer ms.m.Unlock()

	i %= ms.mod
	if watcher == ms.vw[i] || watcher == ms.vcw[i] || nil == watcher {
		return ms.vw[i], true
	}
	return ms.vw[i], false
}

// ValueAt returns the most recent value placed at the argument position.
//
// Returns an error if no value has ever been placed at that position.
func (ms *ModSeq) ValueAt(i uint) (interface{}, error) {
	ms.m.Lock()
	defer ms.m.Unlock()

	i %= ms.mod
	if ms.n == 0 || i > (ms.n-1) {
		return nil, errUnfilledPosition
	}
	return ms.vv[i], nil
}

// ValueAtFor returns, for the argument closed watch channel, the most recent
// value placed at the argument position.
//
// Returns an error if no value has ever been placed at that position or the
// argument watch channel was not the watcher for the value at this position.
// When an error is returned the other return value is nil.
func (ms *ModSeq) ValueAtFor(i uint, watcher <-chan struct{}) (interface{}, error) {
	ms.m.Lock()
	defer ms.m.Unlock()

	if ms.n == 0 || i > (ms.n-1) {
		return nil, errUnfilledPosition
	}
	i %= ms.mod
	if watcher == ms.vw[i] {
		return nil, errUnfilledPosition
	}
	if watcher != ms.vcw[i] {
		return nil, errWatcherNotFound
	}
	return ms.vv[i], nil
}

func (ms *ModSeq) values() []interface{} {
	var values []interface{}
	var i uint = 0
	if isFirstCycle := nil == ms.vcw[ms.i]; !isFirstCycle {
		i = ms.i
	}
	for uint(len(values)) < ms.mod && nil != ms.vcw[i] {
		values = append(values, ms.vv[i])
		i = (i + 1) % ms.mod
	}
	return values
}

// Values returns a new slice containing the currently stored values of the
// sequence ordered by oldest values first.
func (ms *ModSeq) Values() []interface{} {
	ms.m.Lock()
	defer ms.m.Unlock()

	return ms.values()
}

// WatchAt the argument position for the next value to be placed there.
//
// The returned watch channel becomes a closed channel the next time a value is
// placed at the position it watches or when the modulus sequence is closed.
func (ms *ModSeq) WatchAt(i uint) <-chan struct{} {
	ms.m.Lock()
	defer ms.m.Unlock()

	return ms.vw[i%ms.mod]
}

// NewModSeq constructs a new modulo sequence with length given by the argument
// modulus.
//
// The modulus is clamped to the range 1..(largest uint value).
func NewModSeq(mod uint) *ModSeq {
	if mod == 0 {
		mod = 1
	}
	done := make(chan struct{})
	vw := make([]chan struct{}, mod)
	vcw := make([]chan struct{}, mod)
	for i := range vw {
		vw[i] = make(chan struct{})
	}

	return &ModSeq{
		done: done,
		Done: done,
		mod:  mod,
		vcw:  vcw,
		vv:   make([]interface{}, mod),
		vw:   vw,
	}
}

// IsUnfilledPositionError is whether the argument error indicates that no value
// has been placed at that position.
//
// It can mean that no value was ever placed there or that the watcher is still
// an active watcher, that is, not a closed watcher and thus is still watching
// for the next value.
func IsUnfilledPositionError(err error) bool {
	return err == errUnfilledPosition
}

var errUnfilledPosition error = errors.New("unfilled position error indicating either no value or watcher still active")
