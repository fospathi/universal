package watchable_test

import (
	"testing"
	"time"

	"gitlab.com/fospathi/universal/watchable"
)

func TestModPubsub_Pub(t *testing.T) {
	// Publish then close twice then publish.
	{
		ps := watchable.NewModPubsub(1)
		sub1, _, _ := ps.Sub()
		ps.Pub(-1)
		v, ok := <-sub1
		want1, want2 := true, -1
		got1, got2 := ok, v.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		ps.Close()
		ps.Close()
		ps.Pub(-2)
		time.Sleep(10 * time.Millisecond)
		_, ok = <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestModPubsub_Sub(t *testing.T) {
	// Test an empty pubsub.
	{
		ps := watchable.NewModPubsub(1)
		sub1, _, _ := ps.Sub()
		ps.Complete()
		time.Sleep(10 * time.Millisecond)
		_, ok := <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}

	// Subscribe to an empty pubsub then publish then complete.
	{
		ps := watchable.NewModPubsub(1)
		sub1, _, _ := ps.Sub()
		ps.Pub(-1)
		ps.Complete()
		time.Sleep(10 * time.Millisecond)
		v, ok := <-sub1
		want1, want2 := true, -1
		got1, got2 := ok, v.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		_, ok = <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}

	// Publish then subscribe then complete.
	{
		{
			ps := watchable.NewModPubsub(2)
			ps.Pub(-1)
			ps.Pub(-2)
			ps.Pub(-3)
			sub1, _, _ := ps.Sub()
			ps.Complete()
			time.Sleep(10 * time.Millisecond)
			v, ok := <-sub1
			want1, want2 := true, -2
			got1, got2 := ok, v.(int)
			if want1 != got1 || want2 != got2 {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
			}
			v, ok = <-sub1
			want1, want2 = true, -3
			got1, got2 = ok, v.(int)
			if want1 != got1 || want2 != got2 {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
			}
			_, ok = <-sub1
			want := false
			got := ok
			if want != got {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
			ps.Close()
		}
		{
			ps := watchable.NewModPubsub(2)
			ps.Pub(-1)
			ps.Pub(-2)
			ps.Pub(-3)
			ps.Pub(-4)
			ps.Pub(-5)
			sub1, _, _ := ps.Sub()
			ps.Complete()
			time.Sleep(10 * time.Millisecond)
			v, ok := <-sub1
			want1, want2 := true, -4
			got1, got2 := ok, v.(int)
			if want1 != got1 || want2 != got2 {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
			}
			v, ok = <-sub1
			want1, want2 = true, -5
			got1, got2 = ok, v.(int)
			if want1 != got1 || want2 != got2 {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
			}
			_, ok = <-sub1
			want := false
			got := ok
			if want != got {
				t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
			}
			ps.Close()
		}
	}

	// Publish then subscribe then cancel twice then publish then complete.
	{
		ps := watchable.NewModPubsub(2)
		ps.Pub(-1)
		sub1, cancel, _ := ps.Sub()
		time.Sleep(10 * time.Millisecond)
		v, ok := <-sub1
		want1, want2 := true, -1
		got1, got2 := ok, v.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		cancel()
		cancel()
		ps.Pub(-2)
		ps.Complete()
		time.Sleep(10 * time.Millisecond)
		_, ok = <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}

	// Publish then close then subscribe.
	{
		ps := watchable.NewModPubsub(0)
		ps.Pub(-1)
		ps.Close()
		_, _, err := ps.Sub()
		want := true
		got := watchable.IsPubsubClosedError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}
}
