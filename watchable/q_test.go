package watchable_test

import (
	"testing"

	"gitlab.com/fospathi/universal/watchable"
)

func TestBoundedQ(t *testing.T) {
	q := watchable.NewBoundedQ(-3)

	var (
		err error
		w   <-chan struct{}
	)

	_, err = q.Pop()
	{
		want := true
		got := watchable.IsEmptyError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	w = q.Watch()

	select {
	case <-w:
		t.Errorf("watcher should not be closed")
	default:
	}

	if err = q.Add(1); err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	select {
	case <-w:
	default:
		t.Errorf("watcher should be closed")
	}

	{
		want1, want2 := 1, error(nil)
		got1, got2 := q.Pop()
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}

	w = q.Watch()

	select {
	case <-w:
		t.Errorf("watcher should not be closed")
	default:
	}

	if err = q.Add(2); err != nil {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", nil, err)
	}

	err = q.Add(3)
	{
		want := true
		got := watchable.IsFullError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	select {
	case <-w:
	default:
		t.Errorf("watcher should be closed")
	}

	q.Empty()
	_, err = q.Pop()
	{
		want := true
		got := watchable.IsEmptyError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	q.Close()
	err = q.Add(4)
	{
		want := true
		got := watchable.IsClosedError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	q = watchable.NewBoundedQ(2)

	q.Add(5)
	q.Add(6)

	vals := q.PopAllAndClose()
	{
		want1, want2 := 5, 6
		got1, got2 := vals[0].(int), vals[1].(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}

	err = q.Add(7)
	{
		want := true
		got := watchable.IsClosedError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	q = watchable.NewBoundedQ(3)

	q.Add(8)
	q.Add(9)
	q.Add(10)

	w = q.Watch()

	select {
	case <-w:
	default:
		t.Errorf("watcher should be closed")
	}

	q.EmptyAndClose()

	w = q.Watch()

	select {
	case <-w:
		t.Errorf("watcher should not be closed")
	default:
	}

	err = q.Add(11)
	{
		want := true
		got := watchable.IsClosedError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	q = watchable.NewBoundedQ(4)

	q.Add(12)
	q.Add(13)
	q.Add(14)
	q.Add(15)

	v1, _ := q.Pop()
	v2, _ := q.Pop()
	{
		want1, want2 := 12, 13
		got1, got2 := v1.(int), v2.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}

	q.Close()
	q.Close()

	w = q.Watch()

	select {
	case <-w:
	default:
		t.Errorf("watcher should be closed")
	}

	v1, _ = q.Pop()
	v2, _ = q.Pop()
	{
		want1, want2 := 14, 15
		got1, got2 := v1.(int), v2.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
	}
}
