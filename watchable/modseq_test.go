package watchable_test

import (
	"testing"

	"gitlab.com/fospathi/universal/watchable"
)

func TestModSeq_Add(t *testing.T) {
	ms := watchable.NewModSeq(3)

	_, err1 := ms.ValueAt(0)
	_, err2 := ms.ValueAt(1)
	_, err3 := ms.ValueAt(2)

	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	ms.Add(-1)
	ms.Add(-2)
	ms.Add(-3)
	v1, _ := ms.ValueAt(0)
	v2, _ := ms.ValueAt(1)
	v3, _ := ms.ValueAt(2)

	want1, want2, want3 := -1, -2, -3
	got1, got2, got3 := v1, v2, v3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	ms.Add(-4)
	v1, _ = ms.ValueAt(0)
	v2, _ = ms.ValueAt(1)
	v3, _ = ms.ValueAt(2)

	want1, want2, want3 = -4, -2, -3
	got1, got2, got3 = v1, v2, v3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	ms.Add(-5)
	ms.Add(-6)
	ms.Add(-7)
	v1, _ = ms.ValueAt(0)
	v2, _ = ms.ValueAt(1)
	v3, _ = ms.ValueAt(2)

	want1, want2, want3 = -7, -5, -6
	got1, got2, got3 = v1, v2, v3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	v1, _ = ms.ValueAt(3)
	v2, _ = ms.ValueAt(4)
	v3, _ = ms.ValueAt(5)

	got1, got2, got3 = v1, v2, v3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	ms.Close()
	ms.Add(-8)
	ms.Add(-9)
	ms.Add(-10)
	v1, _ = ms.ValueAt(0)
	v2, _ = ms.ValueAt(1)
	v3, _ = ms.ValueAt(2)

	got1, got2, got3 = v1, v2, v3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestModSeq_Position(t *testing.T) {
	ms := watchable.NewModSeq(3)

	want := uint(0)
	got := ms.Position()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	ms.Add(-1)
	got1 := ms.Position()
	ms.Add(-2)
	got2 := ms.Position()
	ms.Add(-3)
	got3 := ms.Position()

	want1, want2, want3 := uint(1), uint(2), uint(0)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	ms.Add(-4)
	got1 = ms.Position()
	ms.Add(-5)
	got2 = ms.Position()
	ms.Add(-6)
	got3 = ms.Position()

	want1, want2, want3 = uint(1), uint(2), uint(0)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	ms = watchable.NewModSeq(1)
	ms.Add(-1)
	got1 = ms.Position()
	ms.Add(-2)
	got2 = ms.Position()
	ms.Add(-3)
	got3 = ms.Position()

	want1, want2, want3 = uint(0), uint(0), uint(0)
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestModSeq_ReWatchAt(t *testing.T) {
	ms := watchable.NewModSeq(3)

	w1, ok1 := ms.ReWatchAt(0, nil)
	w2, ok2 := ms.ReWatchAt(1, nil)
	w3, ok3 := ms.ReWatchAt(2, nil)

	want1, want2, want3 := true, true, true
	got1, got2, got3 := ok1, ok2, ok3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	ms.Add(-1)
	ms.Add(-2)
	ms.Add(-3)
	<-w1
	<-w2
	<-w3

	w1, ok1 = ms.ReWatchAt(0, w1)
	w2, ok2 = ms.ReWatchAt(1, w2)
	w3, ok3 = ms.ReWatchAt(2, w3)

	want1, want2, want3 = true, true, true
	got1, got2, got3 = ok1, ok2, ok3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	ms.Add(-4)
	ms.Add(-5)
	ms.Add(-6)
	<-w1
	<-w2
	<-w3
	ms.Add(-7)
	ms.Add(-8)
	ms.Add(-9)

	_, ok1 = ms.ReWatchAt(0, w1)
	_, ok2 = ms.ReWatchAt(1, w2)
	_, ok3 = ms.ReWatchAt(2, w3)

	want1, want2, want3 = false, false, false
	got1, got2, got3 = ok1, ok2, ok3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestModSeq_ValueAtFor(t *testing.T) {
	ms := watchable.NewModSeq(3)

	w1 := ms.WatchAt(0)
	w2 := ms.WatchAt(1)
	w3 := ms.WatchAt(2)

	{
		_, err1 := ms.ValueAtFor(0, w1)
		_, err2 := ms.ValueAtFor(1, w2)
		_, err3 := ms.ValueAtFor(2, w3)

		{
			want1, want2, want3 := true, true, true
			got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
				watchable.IsUnfilledPositionError(err2),
				watchable.IsUnfilledPositionError(err3)
			if want1 != got1 || want2 != got2 || want3 != got3 {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
			}
		}
	}

	ms.Add(-1)
	ms.Add(-2)
	ms.Add(-3)
	<-w1
	<-w2
	<-w3

	v1, _ := ms.ValueAtFor(0, w1)
	v2, _ := ms.ValueAtFor(1, w2)
	v3, _ := ms.ValueAtFor(2, w3)

	got1, got2, got3 := v1, v2, v3
	want1, want2, want3 := -1, -2, -3
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}

	_, err1 := ms.ValueAtFor(0, w3)
	_, err2 := ms.ValueAtFor(1, w1)
	_, err3 := ms.ValueAtFor(2, w2)

	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := watchable.IsWatcherNotFoundError(err1),
			watchable.IsWatcherNotFoundError(err2),
			watchable.IsWatcherNotFoundError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	w4 := ms.WatchAt(0)
	w5 := ms.WatchAt(1)
	w6 := ms.WatchAt(2)

	{
		_, err1 := ms.ValueAtFor(0, w4)
		_, err2 := ms.ValueAtFor(1, w5)
		_, err3 := ms.ValueAtFor(2, w6)

		{
			want1, want2, want3 := true, true, true
			got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
				watchable.IsUnfilledPositionError(err2),
				watchable.IsUnfilledPositionError(err3)
			if want1 != got1 || want2 != got2 || want3 != got3 {
				t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
			}
		}
	}

	ms.Add(-4)
	ms.Add(-5)
	ms.Add(-6)
	_, err1 = ms.ValueAtFor(0, w1)
	_, err2 = ms.ValueAtFor(1, w2)
	_, err3 = ms.ValueAtFor(2, w3)

	{
		want1, want2, want3 := true, true, true
		got1, got2, got3 := watchable.IsWatcherNotFoundError(err1),
			watchable.IsWatcherNotFoundError(err2),
			watchable.IsWatcherNotFoundError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	<-w4
	<-w5
	<-w6
	v1, _ = ms.ValueAtFor(0, w4)
	v2, _ = ms.ValueAtFor(1, w5)
	v3, _ = ms.ValueAtFor(2, w6)

	got1, got2, got3 = v1, v2, v3
	want1, want2, want3 = -4, -5, -6
	if want1 != got1 || want2 != got2 || want3 != got3 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
	}
}

func TestModSeq_Values(t *testing.T) {
	toInt := func(vi []interface{}) []int {
		var result []int
		for _, i := range vi {
			result = append(result, i.(int))
		}
		return result
	}

	ms := watchable.NewModSeq(0) // Clamped to length 1

	{
		want := 0
		got := len(ms.Values())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms.Add(-1)

	{
		want := []int{-1}
		got := toInt(ms.Values())
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms = watchable.NewModSeq(3)

	{
		want := 0
		got := len(ms.Values())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms.Add(-1)

	{
		want := []int{-1}
		got := toInt(ms.Values())
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms.Add(-2)

	{
		want := []int{-1, -2}
		got := toInt(ms.Values())
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms.Add(-3)

	{
		want := []int{-1, -2, -3}
		got := toInt(ms.Values())
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms.Add(-4)

	{
		want := []int{-2, -3, -4}
		got := toInt(ms.Values())
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms.Add(-5)
	ms.Add(-6)

	{
		want := []int{-4, -5, -6}
		got := toInt(ms.Values())
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	ms.Add(-7)
	ms.Add(-8)

	{
		want := []int{-6, -7, -8}
		got := toInt(ms.Values())
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
