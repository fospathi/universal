package watchable

import (
	"errors"
	"log"
	"net"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

// Pubsub is a concurrency safe publish-subscribe like pattern over a sequence
// of values.
type Pubsub struct {
	done chan struct{}
	m    *sync.Mutex
	seq  *Sequence
	wg   *sync.WaitGroup
}

// Close the pubsub.
//
// Eventually all subscription channels will be closed. Some channels which are
// still consuming available values may, on a random basis, consume more values
// before closing. Close waits until the subscriptions are closed before
// returning.
func (ps Pubsub) Close() {
	ps.m.Lock()
	defer ps.m.Unlock()

	select {
	case <-ps.done:
		return
	default:
	}
	close(ps.done)
	ps.wg.Wait()
}

// Complete indicates that no more new values will be published.
//
// Completing means new and existing subscription channels will close once they
// have consumed all the published values. Each subscription channel has a
// goroutine which, if not cancelled, won't exit until completion or closure.
//
// New subscriptions to a completed pubsub are still allowed and they yield all
// previously published values.
func (ps Pubsub) Complete() {
	ps.seq.Close()
}

// ServeSpec specifies some properties of a new server and a websocket
// handler.
type ServeSpec struct {
	Addr string
	// Set as true to make the websocket handler ignore ports when checking the
	// origin if the origin hostname is a localhost type. This is insecure but
	// makes testing with clients which are being served from a server on
	// localhost but with a different port possible.
	InsecureDev bool
	Mux         *http.ServeMux
	Pattern     string
}

// ListenAndServeOnWebsocket creates a new server with the argument address and
// path pattern and writes published Marshaler type values to clients.
//
// The server writes messages using websockets: published Marshaler values are
// marshalled and then sent as bytes in binary mode, one websocket message per
// value.
//
// To close the server either use the returned cancel function or close the
// receiver pubsub. Completing the pubsub does not close the server, it
// continues serving.
func (ps Pubsub) ListenAndServeOnWebsocket(spec ServeSpec) (CancelFunc, error) {
	var (
		addr     = spec.Addr
		insecure = spec.InsecureDev
		mux      = spec.Mux
		pattern  = spec.Pattern
	)
	if mux == nil {
		mux = http.NewServeMux()
	}
	mux.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		upgrader := websocket.Upgrader{}
		if insecure {
			upgrader.CheckOrigin = func(r *http.Request) bool {
				origin := r.Header["Origin"]
				if len(origin) == 0 {
					return true
				}
				ou, err := url.Parse(origin[0])
				if err != nil {
					return false
				}
				switch ou.Hostname() {
				case "localhost", "127.0.0.1", "::1":
					ru, err := url.Parse(schemify(r.Host))
					if err != nil {
						return false
					}
					return ou.Hostname() == ru.Hostname()
				}
				return strings.EqualFold(ou.Host, r.Host)
			}
		}
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Println("upgrade: ", err)
			return
		}
		defer conn.Close()

		watcher, cancel, err := ps.Sub()
		if err != nil {
			log.Println("sub: ", err)
			return
		}
		defer cancel()

		for v := range watcher {
			if ls, ok := v.(Marshaler); ok {
				b, err := ls.Marshal()
				if err != nil {
					log.Println("marshal: ", err)
					continue
				}

				if err = conn.WriteMessage(websocket.BinaryMessage, b); err != nil {
					log.Println("write value: ", err)
					return
				}
			}
		}

		err = conn.WriteMessage(websocket.CloseMessage,
			websocket.FormatCloseMessage(websocket.CloseNormalClosure, "pubsub complete"))
		if err != nil {
			log.Println("write close: ", err)
			return
		}
		<-time.After(1 * time.Second) // Allow time to write the close message.
	})

	var srv http.Server
	srv.Handler = mux
	srv.Addr = addr
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return func() {}, err
	}
	cancelC := make(chan struct{})
	var cancelM sync.Mutex
	cancel := func() {
		cancelM.Lock()
		defer cancelM.Unlock()
		select {
		case <-cancelC:
			return
		default:
		}
		close(cancelC)
	}
	go func() {
		select {
		case <-ps.done:
		case <-cancelC:
		}
		srv.Close()
	}()
	go func() {
		if err := srv.Serve(ln); err != http.ErrServerClosed {
			log.Println("serve: ", err)
		}
	}()
	return cancel, nil
}

// Pub publishes a value on the pubsub.
//
// Subscribers consume the published values in a first published first consumed
// order.
func (ps Pubsub) Pub(v interface{}) {
	ps.seq.Add(v)
}

// Sub returns a subscription channel which receives all previous and future
// published values.
//
// Cancel the subscription channel prematurely by calling the returned cancel
// function. The subscription channel may, on a random basis, consume more
// values before closing. The cancel function waits until the subscription
// cancellation is accomplished before returning.
//
// If the pubsub has already been closed an error is returned and the other
// return values are nil.
func (ps Pubsub) Sub() (<-chan interface{}, CancelFunc, error) {
	ps.m.Lock()
	defer ps.m.Unlock()
	select {
	case <-ps.done:
		return nil, nil, errPubsubClosed
	default:
	}

	cancelC := make(chan struct{})
	cancelM := &sync.Mutex{}
	cancelWG := &sync.WaitGroup{}
	cancel := func() {
		cancelM.Lock()
		defer cancelM.Unlock()
		select {
		case <-cancelC:
			return
		default:
		}
		close(cancelC)
		cancelWG.Wait()
	}

	c := make(chan interface{})
	ps.wg.Add(1)
	cancelWG.Add(1)
	go func() {
		defer ps.wg.Add(-1)
		defer cancelWG.Add(-1)
		defer close(c)
		w, _ := ps.seq.Watch(nil)
		for {
			select {
			case <-cancelC:
				return

			case <-ps.done:
				return

			case <-w:
				select {
				case <-cancelC:
					return
				default:
				}
				v, err := ps.seq.ValueOf(w)
				if IsNonRemainingError(err) {
					return
				}
				select {
				case <-cancelC:
					return
				case c <- v:
				}
				w, _ = ps.seq.Watch(w)
			}
		}
	}()

	return c, cancel, nil
}

// NewPubsub constructs a new Pubsub.
func NewPubsub() Pubsub {
	return Pubsub{
		done: make(chan struct{}),
		m:    &sync.Mutex{},
		seq:  NewSequence(),
		wg:   &sync.WaitGroup{},
	}
}

// IsPubsubClosedError is whether the argument error indicates that a pubsub was
// already closed.
func IsPubsubClosedError(err error) bool {
	return err == errPubsubClosed
}

var errPubsubClosed error = errors.New("pubsub is closed")

// A CancelFunc informs a task that it should prematurely abandon its work.
type CancelFunc func()

// Marshaler represents an entity which you can marshal into a slice of bytes.
type Marshaler interface {
	// Marshal the receiver entity into a slice of bytes.
	Marshal() ([]byte, error)
}

var schemeRegexp = regexp.MustCompile(`^https?://`)

// Where absent, ensure the argument URL string has a scheme, i.e http://
func schemify(u string) string {
	if !schemeRegexp.MatchString(u) {
		return "http://" + u
	}
	return u
}
