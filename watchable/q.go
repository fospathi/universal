package watchable

import (
	"errors"
	"sync"
)

// BoundedQ is a fixed capacity queue.
type BoundedQ struct {
	// The Done channel is closed by the Close() method.
	Done <-chan struct{}
	done chan struct{}

	capacity int
	m        sync.Mutex
	seq      []interface{}
	w        chan struct{}
}

// Add a value to the end of the queue if there is room.
func (q *BoundedQ) Add(v interface{}) error {
	q.m.Lock()
	defer q.m.Unlock()

	select {
	case <-q.done:
		return errClosed
	default:
	}

	if len(q.seq) >= q.capacity {
		return errFull
	}
	q.seq = append(q.seq, v)

	select {
	case <-q.w:
	default:
		close(q.w)
	}

	return nil
}

func (q *BoundedQ) close() {
	select {
	case <-q.done:
		return
	default:
	}

	close(q.done)
}

// Close the queue.
//
// No more values can be added afterwards.
func (q *BoundedQ) Close() {
	q.m.Lock()
	defer q.m.Unlock()

	q.close()
}

func (q *BoundedQ) empty() {
	if len(q.seq) > 0 {
		q.seq = q.seq[0:0]
	}

	select {
	case <-q.w:
		q.w = make(chan struct{})
	default:
	}
}

// Empty the queue.
func (q *BoundedQ) Empty() {
	q.m.Lock()
	defer q.m.Unlock()

	q.empty()
}

// EmptyAndClose the queue.
func (q *BoundedQ) EmptyAndClose() {
	q.m.Lock()
	defer q.m.Unlock()

	q.empty()
	q.close()
}

func (q *BoundedQ) pop() (interface{}, error) {
	if len(q.seq) == 0 {
		return nil, errEmpty
	}

	v := q.seq[0]
	q.seq = q.seq[1:]
	q.w = make(chan struct{})
	if len(q.seq) > 0 {
		close(q.w)
	}
	return v, nil
}

// Pop a value from the front of the queue.
func (q *BoundedQ) Pop() (interface{}, error) {
	q.m.Lock()
	defer q.m.Unlock()

	return q.pop()
}

// PopAllAndClose the queue.
//
// The returned values are in oldest first order.
func (q *BoundedQ) PopAllAndClose() []interface{} {
	q.m.Lock()
	defer q.m.Unlock()

	seq := q.seq
	q.empty()
	q.close()
	return seq
}

// Watch the queue.
//
// The returned watch channel is closed already if the queue is not empty, else
// it is closed when the next item is added.
func (q *BoundedQ) Watch() <-chan struct{} {
	q.m.Lock()
	defer q.m.Unlock()

	return q.w
}

// NewBoundedQ constructs a new BoundedQ.
//
// The capacity of the queue is clamped to at least 1.
func NewBoundedQ(capacity int) *BoundedQ {
	if capacity < 1 {
		capacity = 1
	}
	done := make(chan struct{})
	return &BoundedQ{
		capacity: capacity,
		done:     done,
		Done:     done,
		m:        sync.Mutex{},
		w:        make(chan struct{}),
	}
}

// IsClosedError is whether the argument error indicates that the thing is
// closed.
func IsClosedError(err error) bool {
	return err == errClosed
}

var errClosed error = errors.New("closed")

// IsEmptyError is whether the argument error indicates that the thing is empty.
func IsEmptyError(err error) bool {
	return err == errEmpty
}

var errEmpty error = errors.New("empty")

// IsFullError is whether the argument error indicates that the thing is full.
func IsFullError(err error) bool {
	return err == errFull
}

var errFull error = errors.New("full up")
