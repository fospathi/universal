package watchable_test

import (
	"testing"
	"time"

	"gitlab.com/fospathi/universal/watchable"
)

func TestIsochronalModSeq_Add(t *testing.T) {
	var delay uint = 10000
	var halfDelay uint = delay / 2
	var modulus uint = 3
	ims := watchable.NewIsochronalModSeq(watchable.Microseconds(delay), modulus, nil)

	ims.Add(-1)
	ims.Add(-2)
	ims.Add(-3)
	ims.Add(-4)
	time.Sleep(time.Duration(delay+halfDelay) * time.Microsecond)
	v1, err1 := ims.ModSeq.ValueAt(0)
	v2, err2 := ims.ModSeq.ValueAt(1)
	v3, err3 := ims.ModSeq.ValueAt(2)

	{
		var noValue interface{} = nil
		want1, want2, want3 := -1, noValue, noValue
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := false, true, true
		got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	time.Sleep(time.Duration(delay) * time.Microsecond)
	v1, err1 = ims.ModSeq.ValueAt(0)
	v2, err2 = ims.ModSeq.ValueAt(1)
	v3, err3 = ims.ModSeq.ValueAt(2)

	{
		var noValue interface{} = nil
		want1, want2, want3 := -1, -2, noValue
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := false, false, true
		got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	time.Sleep(time.Duration(delay) * time.Microsecond)
	v1, err1 = ims.ModSeq.ValueAt(0)
	v2, err2 = ims.ModSeq.ValueAt(1)
	v3, err3 = ims.ModSeq.ValueAt(2)

	{
		want1, want2, want3 := -1, -2, -3
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := false, false, false
		got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	time.Sleep(time.Duration(delay) * time.Microsecond)
	v1, err1 = ims.ModSeq.ValueAt(0)
	v2, err2 = ims.ModSeq.ValueAt(1)
	v3, err3 = ims.ModSeq.ValueAt(2)

	{
		want1, want2, want3 := -4, -2, -3
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := false, false, false
		got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	time.Sleep(time.Duration(delay) * time.Microsecond)
	v1, err1 = ims.ModSeq.ValueAt(0)
	v2, err2 = ims.ModSeq.ValueAt(1)
	v3, err3 = ims.ModSeq.ValueAt(2)

	{
		want1, want2, want3 := -4, -2, -3
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := false, false, false
		got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}

	ims.Close()
	ims.Add(-5)
	time.Sleep(time.Duration(delay) * time.Microsecond)
	v1, err1 = ims.ModSeq.ValueAt(0)
	v2, err2 = ims.ModSeq.ValueAt(1)
	v3, err3 = ims.ModSeq.ValueAt(2)

	{
		want1, want2, want3 := -4, -2, -3
		got1, got2, got3 := v1, v2, v3
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
	{
		want1, want2, want3 := false, false, false
		got1, got2, got3 := watchable.IsUnfilledPositionError(err1),
			watchable.IsUnfilledPositionError(err2),
			watchable.IsUnfilledPositionError(err3)
		if want1 != got1 || want2 != got2 || want3 != got3 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\n", want1, got1, want2, got2, want3, got3)
		}
	}
}
