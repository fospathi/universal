package watchable_test

import (
	"log"
	"net/url"
	"sync"
	"testing"
	"time"

	"github.com/gorilla/websocket"

	"gitlab.com/fospathi/universal/watchable"
)

// mystring implements the watchable.Marshal interface.
type mystring struct {
	s string
}

func (ms mystring) Marshal() ([]byte, error) {
	return []byte(ms.s), nil
}

func CompareSliceOfString(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func TestPubsub_ListenAndServeOnWebsocket(t *testing.T) {
	const addr = "localhost:8080"
	const path = "/"

	wants := []string{"abc", "xyz"}
	gotsM := sync.Mutex{}
	var gots []string

	closedNormallyM := sync.Mutex{}
	var closedNormally bool

	connect := func(onMsg func(msg []byte)) {
		u := url.URL{Scheme: "ws", Host: addr, Path: path}
		conn, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
		if err != nil {
			log.Fatal(err)
		}

		go func() {
			for {
				code, msg, err := conn.ReadMessage()
				if err != nil {
					if e, ok := err.(*websocket.CloseError); ok {
						if e.Code == websocket.CloseNormalClosure {
							closedNormallyM.Lock()
							closedNormally = true
							closedNormallyM.Unlock()
							return
						}
					}
					log.Println("read: ", code, err)
					return
				}
				if nil == onMsg {
					gotsM.Lock()
					gots = append(gots, string(msg))
					gotsM.Unlock()
				} else {
					onMsg(msg)
				}
			}
		}()
	}

	ps := watchable.NewPubsub()
	cancel, err := ps.ListenAndServeOnWebsocket(watchable.ServeSpec{
		Addr:    addr,
		Pattern: path,
	})
	if err != nil {
		log.Fatal(err)
	}
	connect(nil)
	time.Sleep(1 * time.Second)

	ps.Pub(mystring{"abc"})
	ps.Pub("def")
	ps.Pub(mystring{"xyz"})

	time.Sleep(100 * time.Millisecond)

	gotsM.Lock()
	if !CompareSliceOfString(wants, gots) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", wants, gots)
	}
	gotsM.Unlock()

	ps.Complete()
	time.Sleep(100 * time.Millisecond)
	want := true
	closedNormallyM.Lock()
	got := closedNormally
	closedNormallyM.Unlock()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	// Cancel the server. Then create another with the same hostname to test its properly closed.
	cancel()
	time.Sleep(500 * time.Millisecond)

	ps = watchable.NewPubsub()
	cancel, err = ps.ListenAndServeOnWebsocket(watchable.ServeSpec{
		Addr:    addr,
		Pattern: path,
	})
	if err != nil {
		log.Fatal(err)
	}
	gots = []string{}
	connect(func(msg []byte) {
		gotsM.Lock()
		gots = append(gots, string(msg))
		gotsM.Unlock()
	})
	ps.Pub(mystring{"pqr"})
	time.Sleep(1 * time.Second)

	gotsM.Lock()
	wants = []string{"pqr"}
	if !CompareSliceOfString(wants, gots) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", wants, gots)
	}
	gotsM.Unlock()

	ps.Complete()
	cancel()
}

func TestPubsub_Pub(t *testing.T) {
	// Publish then close twice then publish.
	{
		ps := watchable.NewPubsub()
		sub1, _, _ := ps.Sub()
		ps.Pub(-1)
		v, ok := <-sub1
		want1, want2 := true, -1
		got1, got2 := ok, v.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		ps.Close()
		ps.Close()
		ps.Pub(-2)
		time.Sleep(10 * time.Millisecond)
		_, ok = <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestPubsub_Sub(t *testing.T) {
	// Test an empty pubsub.
	{
		ps := watchable.NewPubsub()
		sub1, _, _ := ps.Sub()
		ps.Complete()
		time.Sleep(10 * time.Millisecond)
		_, ok := <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}

	// Subscribe to an empty pubsub then publish then complete.
	{
		ps := watchable.NewPubsub()
		sub1, _, _ := ps.Sub()
		ps.Pub(-1)
		ps.Complete()
		time.Sleep(10 * time.Millisecond)
		v, ok := <-sub1
		want1, want2 := true, -1
		got1, got2 := ok, v.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		_, ok = <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}

	// Publish then subscribe then complete.
	{
		ps := watchable.NewPubsub()
		ps.Pub(-1)
		sub1, _, _ := ps.Sub()
		ps.Complete()
		time.Sleep(10 * time.Millisecond)
		v, ok := <-sub1
		want1, want2 := true, -1
		got1, got2 := ok, v.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		_, ok = <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}

	// Publish then subscribe then cancel twice then publish then complete.
	{
		ps := watchable.NewPubsub()
		ps.Pub(-1)
		sub1, cancel, _ := ps.Sub()
		time.Sleep(10 * time.Millisecond)
		v, ok := <-sub1
		want1, want2 := true, -1
		got1, got2 := ok, v.(int)
		if want1 != got1 || want2 != got2 {
			t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
		}
		cancel()
		cancel()
		ps.Pub(-2)
		ps.Complete()
		time.Sleep(10 * time.Millisecond)
		_, ok = <-sub1
		want := false
		got := ok
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}

	// Publish then close then subscribe.
	{
		ps := watchable.NewPubsub()
		ps.Pub(-1)
		ps.Close()
		_, _, err := ps.Sub()
		want := true
		got := watchable.IsPubsubClosedError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		ps.Close()
	}
}
