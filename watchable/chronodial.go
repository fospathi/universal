package watchable

import (
	"sync"
	"time"
)

// A ChronoDial lets you watch for chrono shifts on its dial positions which are
// analogous to the numbers on an analogue clock face.
//
// A single clock hand moves around the clock face depositing a chrono shift
// value on a dial position each time it leaves it.
type ChronoDial struct {
	ims       *IsochronalModSeq
	period    uint
	positions Dozens
}

// AtPosition returns the most recent chrono shift placed at the argument dial
// position.
//
// Returns an error if no chrono shift has ever been placed at that dial
// position.
func (cd ChronoDial) AtPosition(pos uint) (ChronoShift, error) {
	v, err := cd.ims.ModSeq.ValueAt(pos)
	if err != nil {
		return ChronoShift{}, err
	}
	cs := v.(ChronoShift)
	return cs, err
}

// AtPositionFor returns, for the argument closed watch channel, the most recent
// chrono shift placed at the argument dial position.
//
// Returns an error if no chrono shift has ever been placed at that position or
// the argument watch channel was not the watcher for the chrono shift at this
// position.
func (cd ChronoDial) AtPositionFor(pos uint, watcher <-chan struct{}) (ChronoShift, error) {
	v, err := cd.ims.ModSeq.ValueAtFor(pos, watcher)
	if err != nil {
		return ChronoShift{}, err
	}
	cs := v.(ChronoShift)
	return cs, err
}

// AtOclock returns the most recent chrono shift placed at the argument oclock
// position.
//
// Returns an error if no chrono shift has ever been placed at that oclock
// position.
func (cd ChronoDial) AtOclock(oclock uint) (ChronoShift, error) {
	v, err := cd.ims.ModSeq.ValueAt(oclock * uint(cd.positions))
	if err != nil {
		return ChronoShift{}, err
	}
	cs := v.(ChronoShift)
	return cs, err
}

// AtOclockFor returns, for the argument closed watch channel, the most recent
// chrono shift placed at the argument oclock position.
//
// Returns an error if no chrono shift has ever been placed at that position or
// the argument watch channel was not the watcher for the chrono shift at this
// position.
func (cd ChronoDial) AtOclockFor(oclock uint, watcher <-chan struct{}) (ChronoShift, error) {
	v, err := cd.ims.ModSeq.ValueAtFor(oclock*uint(cd.positions), watcher)
	if err != nil {
		return ChronoShift{}, err
	}
	cs := v.(ChronoShift)
	return cs, err
}

// Close closes the chrono dial and all watch channels.
func (cd ChronoDial) Close() {
	cd.ims.Close()
}

// Period is the idealised time taken for the chronodial's clock hand to make
// one complete revolution around the dial.
func (cd ChronoDial) Period() time.Duration {
	return time.Duration(cd.period) * time.Microsecond
}

// Position on the dial where the next chrono shift will be placed.
//
// The hand position on an analogous physical analogue clock would be one behind
// this position.
func (cd ChronoDial) Position() uint {
	return cd.ims.ModSeq.Position()
}

// ReWatch re-watches the argument dial position for the next chrono shift
// to be placed there and checks whether the argument watcher is current.
//
// The returned watch channel becomes a closed channel when a chrono shift value
// is eventually placed there or when the chrono dial is closed.
//
// Return false if intervening chrono shift values may have been missed, that
// is, if the argument watch channel is neither the preceding nor the active
// watch channel at this position.
//
// Always returns true if the argument watch channel is nil.
func (cd ChronoDial) ReWatch(pos uint, watcher <-chan struct{}) (<-chan struct{}, bool) {
	return cd.ims.ModSeq.ReWatchAt(pos, watcher)
}

// ReWatchOclock re-watches the dial position congruent with the argument
// o'clock clock position for the next chrono shift value to be placed there and
// checks whether the argument watcher is current.
//
// The returned watch channel becomes a closed channel when a chrono shift value
// is eventually placed there or when the chrono dial is closed.
//
// Return false if intervening chrono shift values may have been missed, that
// is, if the argument watch channel is neither the preceding nor the active
// watch channel at this position.
//
// Always returns true if the argument watch channel is nil.
func (cd ChronoDial) ReWatchOclock(oclock uint, watcher <-chan struct{}) (<-chan struct{}, bool) {
	return cd.ims.ModSeq.ReWatchAt(oclock*uint(cd.positions), watcher)
}

// Watch watches the argument dial position for the next chrono shift value to
// be placed there.
//
// The returned watch channel becomes a closed channel when a chrono shift value
// is eventually placed there or when the chrono dial is closed.
func (cd ChronoDial) Watch(pos uint) <-chan struct{} {
	return cd.ims.ModSeq.WatchAt(pos)
}

// WatchOclock watches the dial position congruent with the argument o'clock
// clock position for the next chrono shift value to be placed there.
//
// The returned watch channel becomes a closed channel when a chrono shift value
// is eventually placed there or when the chrono dial is closed.
func (cd ChronoDial) WatchOclock(oclock uint) <-chan struct{} {
	return cd.ims.ModSeq.WatchAt(oclock * uint(cd.positions))
}

// NewChronoDial constructs a new ChronoDial.
//
// The dial positions and resolution arguments are clamped to the range [1,
// largest uint value].
func NewChronoDial(spec ChronoDialSpec) ChronoDial {
	if spec.Dozens == 0 {
		spec.Dozens = 1
	}
	if spec.Resolution == 0 {
		spec.Resolution = 1
	}
	{
		var (
			positions   uint        = uint(spec.Dozens) * 12
			resolution  uint        = uint(spec.Resolution)
			period      uint        = uint(resolution) * uint(positions)
			vt          []time.Time = make([]time.Time, positions)
			t0          time.Time
			isT0        bool = true
			periodCount uint = 0
			pos         uint = 0
			m           sync.Mutex
		)
		onEmpty := func(ims *IsochronalModSeq) {
			m.Lock()
			defer m.Unlock()
			pos %= positions
			t := time.Now()
			if isT0 {
				t0 = t
			}
			var prevPeriodT time.Time
			if periodCount == 0 {
				prevPeriodT = t0
			} else {
				prevPeriodT = vt[pos]
			}
			var prevPosT time.Time
			if periodCount == 0 && pos == 0 {
				prevPosT = t0
			} else {
				var prevPos uint
				if pos == 0 {
					prevPos = positions - 1
				} else {
					prevPos = pos - 1
				}
				prevPosT = vt[prevPos]
			}
			cs := ChronoShift{
				IdealMonotonic:     time.Duration(period*periodCount+resolution*pos) * time.Microsecond,
				IdealPeriod:        time.Duration(period) * time.Microsecond,
				IdealResolution:    time.Duration(resolution) * time.Microsecond,
				MeasuredMonotonic:  t.Sub(t0),
				MeasuredPeriod:     t.Sub(prevPeriodT),
				MeasuredResolution: t.Sub(prevPosT),

				Position:      pos,
				IsFirstPeriod: periodCount == 0,
				IsT0:          isT0,
				PeriodCount:   periodCount,
				T0:            t0,
				Time:          t,
			}
			if isT0 && spec.Immediate {
				ims.ModSeq.Add(cs)
			} else {
				ims.Add(cs)
			}
			vt[pos] = t
			if pos == positions-1 {
				periodCount++
			}
			pos++
			isT0 = false
		}
		ims := NewIsochronalModSeq(Microseconds(resolution), positions, onEmpty)
		if spec.Immediate {
			onEmpty(ims)
		}
		return ChronoDial{
			positions: spec.Dozens,
			ims:       ims,
			period:    period,
		}
	}
}

// Dozens is how many twelves of something.
type Dozens uint

// Microseconds is how many microseconds of a duration.
type Microseconds uint

// ChronoDialSpec specifies the properties of a new ChronoDial.
type ChronoDialSpec struct {
	// The multiple-of-twelve number of dial positions on the chrono dial's
	// clockface.
	//
	// It's always a multiple of twelve to facilitate the existence of the
	// chrono dial's oclock methods.
	Dozens Dozens
	// The ideal duration the hand stays at one dial position.
	Resolution Microseconds
	// True if the first chrono shift should be added to the chrono dial
	// immediately upon construction. False to wait for a duration of one
	// resolution.
	Immediate bool
}

// A ChronoShift contains timing information for a chrono dial's dial position.
//
// For chrono shifts in the first period the MeasuredPeriod value should not be
// relied upon. Similarly for the t0 (first ever) chrono shift the
// MeasuredResolution value should not be relied upon.
type ChronoShift struct {
	IdealPeriod        time.Duration // The ideal duration since this hand's chrono shift in the previous period.
	IdealMonotonic     time.Duration // The ideal duration since t0.
	IdealResolution    time.Duration // The ideal duration since the chrono shift before this.
	MeasuredPeriod     time.Duration // The measured duration since this hand's chrono shift in the previous period.
	MeasuredMonotonic  time.Duration // The measured duration since t0.
	MeasuredResolution time.Duration // The measured duration since the chrono shift before this.

	Position      uint      // The dial position of this chrono shift.
	IsFirstPeriod bool      // True if this is during the chrono dial's first revolution around the clock face.
	IsT0          bool      // True if this is the chrono dial's first ever chrono shift.
	PeriodCount   uint      // The whole number of revolutions around the clock face completed so far.
	T0            time.Time // The time of the chrono dial's first ever chrono shift.
	Time          time.Time // The time of this chrono shift.
}

// DeltaT is the measured time since this dial position's chrono shift in the
// previous period as a float number of seconds.
//
// For the first period do not rely on this value.
func (cs ChronoShift) DeltaT() float64 {
	return float64(cs.MeasuredPeriod) * 1e-9
}

// T is the measured time since t0 as a float number of seconds.
func (cs ChronoShift) T() float64 {
	return float64(cs.MeasuredMonotonic) * 1e-9
}

// A PPS60 contains some chrono dial specs.
type PPS60 struct {
	Twelve, Sixty, ThreeSixty ChronoDialSpec
}

// A convenient collection of chrono dial specs that specify chrono dials with
// periods of close to (1/60) seconds and varying numbers of dial positions.
var (
	// 12 positions, 16.67 milliseconds period, 1389 microsecond resolution.
	//
	// PPS == periods per second
	PPS60Pos12 = ChronoDialSpec{Dozens: 1, Resolution: 1389}

	// 60 positions, 16.68 milliseconds period, 278 microsecond resolution.
	//
	// PPS == periods per second
	PPS60Pos60 = ChronoDialSpec{Dozens: 5, Resolution: 278}

	// 360 positions, 16.56 milliseconds period, 46 microsecond resolution.
	//
	// PPS == periods per second
	PPS60Pos360 = ChronoDialSpec{Dozens: 30, Resolution: 46}
)

// A convenient collection of chrono dial specs that specify chrono dials with
// periods of close to (1/30) seconds and varying numbers of dial positions.
var (
	// 12 positions, 33 milliseconds period, 2778 microsecond resolution.
	//
	// PPS == periods per second
	PPS30Pos12 = ChronoDialSpec{Dozens: 1, Resolution: 2778}
)

// A convenient collection of chrono dial specs that specify chrono dials with
// periods of close to (1/5) seconds and varying numbers of dial positions.
var (
	// 12 positions, 200 milliseconds period, 16667 microsecond resolution.
	//
	// PPS == periods per second
	PPS5Pos12 = ChronoDialSpec{Dozens: 1, Resolution: 16667}
)
