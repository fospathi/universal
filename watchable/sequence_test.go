package watchable_test

import (
	"sync"
	"testing"
	"time"

	"gitlab.com/fospathi/universal/watchable"
)

func TestSequence_Add(t *testing.T) {
	seq := watchable.NewSequence()

	for i := 0; i < 4; i++ {
		seq.Add(i)
	}

	vv := make([]int, 4)
	for i, v := range seq.Values() {
		vv[i] = v.(int)
	}

	want := true
	got := CompareSliceOfInt(vv, []int{0, 1, 2, 3})
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Close()
	seq.Add(1)
	{
		want := 4
		got := len(seq.Values())
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}

func TestSequence_AddAll(t *testing.T) {
	seq := watchable.NewSequence()
	seq.AddAll([]interface{}{-1, -2})

	want := 2
	got := len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	watcher, _ := seq.Watch(nil)
	v, _ := seq.ValueOf(watcher)
	want = -1
	got = v.(int)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	watcher, _ = seq.Watch(watcher)
	v, _ = seq.ValueOf(watcher)
	want = -2
	got = v.(int)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	watcher, _ = seq.Watch(watcher)
	_, err := seq.ValueOf(watcher)
	{
		want := true
		got := watchable.IsNonRemainingError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	seq.AddAll([]interface{}{-3})
	v, _ = seq.ValueOf(watcher)
	want = -3
	got = v.(int)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Close()
	seq.AddAll([]interface{}{-4, -5})
	want = 3
	got = len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSequence_Close(t *testing.T) {
	seq := watchable.NewSequence()
	seq.Add(5)
	// Test multiple closes.
	seq.Close()
	seq.Close()
	seq.Add(7)

	want := 1
	got := len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	watcher, _ := seq.Watch(nil)
	v, _ := seq.ValueOf(watcher)
	want = 5
	got = v.(int)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSequence_RemoveFor(t *testing.T) {
	seq := watchable.NewSequence()

	watcher, _ := seq.Watch(nil)
	_, err := seq.RemoveFor(watcher)
	{
		want := true
		got := watchable.IsNonRemainingError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	_, err = seq.RemoveFor(nil)
	{
		want := true
		got := watchable.IsWatcherNotFoundError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	seq.AddAll([]interface{}{-1})
	v, _ := seq.RemoveFor(watcher)
	want := -1
	got := v.(int)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 0
	got = len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.AddAll([]interface{}{-2, -3})

	_, err = seq.RemoveFor(watcher)
	{
		want := true
		got := watchable.IsWatcherNotFoundError(err)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	w1, _ := seq.Watch(nil)
	w2, _ := seq.Watch(w1)
	v1, _ := seq.RemoveFor(w1)
	v2, _ := seq.RemoveFor(w2)
	want1, want2 := -2, -3
	got1, got2 := v1, v2
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want = 0
	got = len(seq.Values())
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestSequence_Watch(t *testing.T) {
	seq := watchable.NewSequence()
	destM := sync.Mutex{}
	var dest []int
	go func() {
		var prev <-chan struct{}
		var w <-chan struct{}
		for {
			w, _ = seq.Watch(prev)
			<-w
			select {
			case <-seq.Done:
				destM.Lock()
				dest = append(dest, 9)
				destM.Unlock()
				return
			default:
			}
			v, _ := seq.ValueOf(w)
			destM.Lock()
			dest = append(dest, v.(int))
			destM.Unlock()
			prev = w
		}
	}()

	seq.Add(5)
	seq.Add(6)
	seq.Add(7)
	seq.Add(8)
	// Give time for the watch channels to close and the watch loop to add the values to dest.
	time.Sleep(20 * time.Millisecond)
	seq.Close()
	// Give time for the last unfilled watch channel to close and the watch loop to add the value to dest.
	time.Sleep(20 * time.Millisecond)

	destM.Lock()
	want := []int{5, 6, 7, 8, 9}
	got := dest
	if !CompareSliceOfInt(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
	destM.Unlock()

	{
		seq := watchable.NewSequence()
		seq.Add(-1)
		seq.Add(-2)
		seq.Close()
		time.Sleep(50 * time.Millisecond)
		dest = []int{}
		go func() {
			var prev <-chan struct{}
			var w <-chan struct{}
			var err error
		forLoop:
			for {
				w, _ = seq.Watch(prev)
				<-w
				var v interface{}
				v, err = seq.ValueOf(w)
				if watchable.IsNonRemainingError(err) {
					break forLoop
				}
				destM.Lock()
				dest = append(dest, v.(int))
				destM.Unlock()
				prev = w
			}
		}()
		time.Sleep(50 * time.Millisecond)

		destM.Lock()
		want = []int{-1, -2}
		got = dest
		if !CompareSliceOfInt(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
		destM.Unlock()
	}
}

func TestIsNotFoundError(t *testing.T) {
	seq := watchable.NewSequence()

	_, err := seq.ValueOf(nil)
	want := true
	got := watchable.IsWatcherNotFoundError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	_, err = seq.Watch(make(<-chan struct{}))
	want = true
	got = watchable.IsWatcherNotFoundError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Add(1)

	_, err = seq.Watch(make(<-chan struct{}))
	want = true
	got = watchable.IsWatcherNotFoundError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestIsNoMoreError(t *testing.T) {
	seq := watchable.NewSequence()

	watcher, _ := seq.Watch(nil)
	_, err := seq.Watch(watcher)

	want := true
	got := watchable.IsNonRemainingError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	_, err = seq.ValueOf(watcher)
	got = watchable.IsNonRemainingError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	seq.Add(1)

	watcher, _ = seq.Watch(watcher)
	_, err = seq.ValueOf(watcher)
	got = watchable.IsNonRemainingError(err)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func CompareSliceOfInt(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
