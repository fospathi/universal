package interval

import "math"

// PiIn is an interval from -Pi to +Pi.
var PiIn = In{-math.Pi, math.Pi}

// NewPi returns a new Pi trig interval using the argument angles, measured in
// radians.
//
// The argument values are clamped to the range -Pi <= angle <= Pi.
func NewPi(from, to float64) Pi {
	return Pi{PiIn.Clamp(from), PiIn.Clamp(to)}
}

// Pi is a trig angle interval anticlockwise from the first to the second angle
// element, measured in radians, with values not greater than Pi in magnitude.
//
// Numerically speaking the interval is not necessarily continuous. Usually when
// the start is greater than the finish then there is a discontinuity in the
// interval at Pi. Abstractly speaking the interval is always continuous.
//
// The angle should be less than two Pi radians except in the case -Pi to Pi
// which is the canonical way to represent a complete revolution.
//
// Two equal interval limits represent an interval whose angle is zero.
type Pi [2]float64

// Angle is the magnitude of the anticlockwise angle covered by the receiver
// trig interval.
func (in Pi) Angle() float64 {
	if in.IsContinuous() {
		if in[0] == math.Pi && in[1] == -math.Pi {
			return 0
		}
		return in[1] - in[0]
	}
	return (math.Pi - in[0]) + (math.Pi + in[1])
}

// Complement returns the other part of the possible interval space that is not
// occupied by the receiver interval.
//
// The special cases are:
//
// * if the receiver interval has an angle of zero then the returned complement
// is the canonical full revolution representation of -Pi to Pi.
//
// * if the receiver interval has an angle of exactly two Pi then the returned
// complement is an empty revolution representation of 0 to 0.
func (in Pi) Complement() Pi {
	switch in.Angle() {
	case 0:
		return Pi{-math.Pi, math.Pi}
	case 2 * math.Pi:
		return Pi{0, 0}
	}
	return Pi{in[1], in[0]}
}

// AsClosedContains returns whether the argument angle value, measured in
// radians, is contained in the closed receiver interval.
//
// The special cases are:
//
// * when the receiver's angle value is zero, such that the receiver interval is
// empty, false is always returned.
func (in Pi) AsClosedContains(v float64) bool {
	if in.Angle() > 0 {
		for _, ivl := range in.Intervals() {
			if ivl.AsClosedContains(v) {
				return true
			}
		}
	}
	return false
}

// Containing returns a pi trig interval with the same limits as the receiver
// containing the argument angle which shall be measured in radians and in the
// range -Pi to Pi.
//
// The argument angle shall not coincide with an interval limit.
func (in Pi) Containing(a float64) Pi {
	if in.AsClosedContains(a) {
		return in
	}
	return in.Complement()
}

// Interval returns the interval that is equivalent to the receiver trig
// interval.
//
// Interval prefers to return an interval whose limits are in the range -Pi to
// Pi but when this isn't possible the returned interval's endpoint can be
// greater than Pi.
func (in Pi) Interval() In {
	if in.IsContinuous() {
		if math.Pi == in[0] {
			return In{-math.Pi, in[1]}
		}
		return In{in[0], in[1]}
	}
	return In{in[0], math.Pi + (in[1] + math.Pi)}
}

// Intervals returns the intervals, where each one is numerically continuous,
// that together are equivalent to the receiver trig interval.
//
// The returned intervals are in the range -Pi to Pi.
func (in Pi) Intervals() []In {
	if in.IsContinuous() {
		if math.Pi == in[0] {
			return []In{{-math.Pi, in[1]}}
		}
		return []In{{in[0], in[1]}}
	}
	return []In{{in[0], math.Pi}, {-math.Pi, in[1]}}
}

// IsContinuous returns whether the receiver trig interval can be represented as
// a single continuous interval in the range -Pi to Pi.
func (in Pi) IsContinuous() bool {
	if in[0] != math.Pi && in[0] > in[1] {
		return false
	}
	return true
}

// NotContaining returns a pi trig interval with the same limits as the receiver
// which does not contain the argument angle which shall be measured in radians
// and in the range -Pi to Pi.
//
// The argument angle shall not coincide with an interval limit.
func (in Pi) NotContaining(a float64) Pi {
	if in.AsClosedContains(a) {
		return in.Complement()
	}
	return in
}
