/*
Package interval contains primitives for working with basic numeric intervals.
*/
package interval

// In represents a directed interval on the real line from the first element to
// the second element.
type In [2]float64

// AbsLen is the length of the interval's domain.
func (in1 In) AbsLen() float64 {
	if in1[0] < in1[1] {
		return in1[1] - in1[0]
	}
	return in1[0] - in1[1]
}

// AsClosedContains reports whether the closed interval contains the argument
// value.
func (in1 In) AsClosedContains(v float64) bool {
	min, max := in1.MinMax()
	return v >= min && v <= max
}

// AsOpenContains reports whether the open interval contains the argument value.
func (in1 In) AsOpenContains(v float64) bool {
	min, max := in1.MinMax()
	return v > min && v < max
}

// AsOpenIntersection reports whether two non-zero length open intervals overlap
// and in the affirmative case returns the intersection of the intervals.
//
// The returned intersection, if it exists, has the same direction as the
// receiver interval.
//
// Returns false if there is no intersection.
func (in1 In) AsOpenIntersection(in2 In) (In, bool) {
	if in1 == in2 {
		return in1, true
	}
	forward := in1[0] < in1[1]
	a1, b1 := in1.MinMax()
	a2, b2 := in2.MinMax()

	hasA, hasB := (a2 > a1) && (a2 < b1), (b2 > a1) && (b2 < b1)
	switch {
	case hasA && hasB:
		//   -----------<--------------->-----------
		//              a1             b1
		//
		//                  <------->
		//                  a2     b2
		if forward {
			return In{a2, b2}, true
		}
		return In{b2, a2}, true

	case hasA:
		//  -----------<--------------->-----------
		//             a1             b1
		//
		//                         <------->
		//                         a2     b2
		if forward {
			return In{a2, b1}, true
		}
		return In{b1, a2}, true

	case hasB:
		//  -----------<--------------->-----------
		//             a1             b1
		//
		//         <------->
		//         a2     b2
		if forward {
			return In{a1, b2}, true
		}
		return In{b2, a1}, true

	case (a1 >= a2) && (a1 < b2) && (b1 > a2) && (b1 <= b2):
		//   -----------<--------------->-----------
		//              a2             b2
		//
		//                  <------->
		//                  a1     b1
		return in1, true

	default:
		return In{}, false
	}
}

// Clamp the given value to the interval's domain.
func (in1 In) Clamp(v float64) float64 {
	min, max := in1.MinMax()
	switch {
	case v < min:
		return min
	case v > max:
		return max
	}
	return v
}

// ClampedT is the interval value at the argument proportion, in the range 0..1,
// of the interval domain.
//
// A t of 0 or less returns the interval's first element. A t of 1 or greater
// returns the interval's second element.
func (in1 In) ClampedT(t float64) float64 {
	if t >= 1 {
		return in1[1]
	} else if t <= 0 {
		return in1[0]
	}
	return in1[0] + t*(in1[1]-in1[0])
}

// ContractLimits returns an interval with the same midpoint but with each of
// the interval limits moved closer to the midpoint by the given distance.
//
// If the interval's length is less than or equals twice the given distance then
// the returned interval is a zero length interval.
func (in1 In) ContractLimits(d float64) In {
	if d < 0 {
		d = -d
	}
	if in1.AbsLen() <= 2*d {
		m := in1.Midpoint()
		return In{m, m}
	}
	if forward := in1[0] < in1[1]; forward {
		return In{in1[0] + d, in1[1] - d}
	}
	return In{in1[0] - d, in1[1] + d}
}

// Midpoint of the interval.
func (in1 In) Midpoint() float64 {
	return (in1[0] + in1[1]) / 2
}

// MinMax interval limits.
func (in1 In) MinMax() (float64, float64) {
	if in1[0] > in1[1] {
		return in1[1], in1[0]
	}
	return in1[0], in1[1]
}

// Opposite direction interval to the receiver with the same domain.
func (in1 In) Opposite() In {
	return In{in1[1], in1[0]}
}

// Split the interval into argument almost equally sized sub intervals.
//
// The sub intervals are in the same direction as the receiver interval.
//
// If the interval has zero length, the return value is nil.
func (in1 In) Split(n int) []In {
	if in1[0]-in1[1] == 0 {
		return nil
	}
	vin := make([]In, n)
	vin[0][0] = in1[0]
	vin[n-1][1] = in1[1]
	Δ := (in1[1] - in1[0]) / float64(n)
	for i := 1; i < n; i++ {
		x := in1[0] + Δ*float64(i)
		vin[i-1][1] = x
		vin[i][0] = x
	}
	return vin
}

// T is the interval value at the argument proportion, usually in the range
// 0..1, of the interval domain.
func (in1 In) T(t float64) float64 {
	if t == 1 {
		return in1[1]
	}
	return in1[0] + t*(in1[1]-in1[0])
}

// TAtX is the proportion of the interval width (t) for the given value in the
// interval domain.
func (in1 In) TAtX(x float64) float64 {
	if x == in1[1] {
		return 1
	}
	return (x - in1[0]) / (in1[1] - in1[0])
}

// TSub is the subinterval between the argument proportions such that 0 in the
// argument interval corresponds to the receiver's first element and 1
// corresponds to the second element.
func (in1 In) TSub(t In) In {
	return In{in1.T(t[0]), in1.T(t[1])}
}
