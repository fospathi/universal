package interval

import (
	"fmt"
	"math"
	"reflect"
	"strconv"
	"testing"
)

func Test_Pi(t *testing.T) {
	format4 := "\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\nWant3:\n%#v\nGot3:\n%#v\nWant4:\n%#v\nGot4:\n%#v\n"

	const TwoPi = math.Pi * 2

	compareFloat64 := func(f1 float64, f2 float64, precision int) bool {
		s1 := fmt.Sprintf("%.*f", precision, f1)
		s2 := fmt.Sprintf("%.*f", precision, f2)
		r1, _ := strconv.ParseFloat(s1, 64)
		r2, _ := strconv.ParseFloat(s2, 64)
		return r1 == r2
	}

	const pi = math.Pi
	a60 := pi / 3
	a120 := 2 * pi / 3
	broken := Pi{a120, -a120}
	neg := Pi{-a120, -a60}
	pos := Pi{a60, a120}
	negPos := Pi{-a60, a60}

	want1, want2, want3, want4 := a120, a60, a60, a120
	got1, got2, got3, got4 := broken.Angle(), neg.Angle(), pos.Angle(), negPos.Angle()
	if !compareFloat64(want1, got1, 4) || !compareFloat64(want2, got2, 4) ||
		!compareFloat64(want3, got3, 4) || !compareFloat64(want4, got4, 4) {
		t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
	}

	want1, want2, want3, want4 = TwoPi-a120, TwoPi-a60, TwoPi-a60, TwoPi-a120
	got1, got2, got3, got4 = broken.Complement().Angle(), neg.Complement().Angle(),
		pos.Complement().Angle(), negPos.Complement().Angle()
	if !compareFloat64(want1, got1, 4) || !compareFloat64(want2, got2, 4) ||
		!compareFloat64(want3, got3, 4) || !compareFloat64(want4, got4, 4) {
		t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
	}

	{
		want1, want2, want3, want4 := false, true, true, true
		got1, got2, got3, got4 := broken.IsContinuous(), neg.IsContinuous(),
			pos.IsContinuous(), negPos.IsContinuous()
		if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
			t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
		}

		want1, want2, want3, want4 = true, false, false, false
		got1, got2, got3, got4 = broken.Complement().IsContinuous(), neg.Complement().IsContinuous(),
			pos.Complement().IsContinuous(), negPos.Complement().IsContinuous()
		if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
			t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
		}

		want1, want2, want3, want4 = true, true, true, true
		got1, got2, got3, got4 = broken.AsClosedContains(math.Pi), neg.AsClosedContains(-math.Pi/2),
			pos.AsClosedContains(a120), negPos.AsClosedContains(0)
		if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
			t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
		}

		want1, want2, want3, want4 = false, false, true, false
		got1, got2, got3, got4 = broken.Complement().AsClosedContains(math.Pi), neg.Complement().AsClosedContains(-math.Pi/2),
			pos.Complement().AsClosedContains(a120), negPos.Complement().AsClosedContains(0)
		if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
			t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
		}
	}

	{
		full, empty1, empty2, empty3 := Pi{-math.Pi, math.Pi}, Pi{math.Pi, -math.Pi},
			Pi{-math.Pi, -math.Pi}, Pi{math.Pi, math.Pi}
		want1, want2, want3, want4 := TwoPi, 0.0, 0.0, 0.0
		got1, got2, got3, got4 := full.Angle(), empty1.Angle(), empty2.Angle(), empty3.Angle()
		if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
			t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
		}

		full, empty1, empty2, empty3 = Pi{-math.Pi, math.Pi}, Pi{math.Pi, -math.Pi},
			Pi{-math.Pi, -math.Pi}, Pi{math.Pi, math.Pi}
		want1, want2, want3, want4 = 0.0, TwoPi, TwoPi, TwoPi
		got1, got2, got3, got4 = full.Complement().Angle(), empty1.Complement().Angle(),
			empty2.Complement().Angle(), empty3.Complement().Angle()
		if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
			t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
		}

		{
			want1, want2, want3, want4 := true, true, true, true
			got1, got2, got3, got4 := full.IsContinuous(), empty1.IsContinuous(),
				empty2.IsContinuous(), empty3.IsContinuous()
			if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
				t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
			}

			want1, want2, want3, want4 = true, true, true, true
			got1, got2, got3, got4 = full.Complement().IsContinuous(), empty1.Complement().IsContinuous(),
				empty2.Complement().IsContinuous(), empty3.Complement().IsContinuous()
			if want1 != got1 || want2 != got2 || want3 != got3 || want4 != got4 {
				t.Errorf(format4, want1, got1, want2, got2, want3, got3, want4, got4)
			}
		}
	}

	{
		want := false
		got := Pi{0, 0}.AsClosedContains(0)
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := In{math.Pi / 2, math.Pi + math.Pi/2}
		got := Pi{math.Pi / 2, -math.Pi / 2}.Interval()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want = In{-math.Pi, -math.Pi / 2}
		got = Pi{math.Pi, -math.Pi / 2}.Interval()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}

		want = In{-math.Pi, math.Pi / 2}
		got = Pi{math.Pi, math.Pi / 2}.Interval()
		if want != got {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}

	{
		want := []In{{math.Pi / 2, math.Pi}, {-math.Pi, -math.Pi / 2}}
		got := Pi{math.Pi / 2, -math.Pi / 2}.Intervals()
		if !reflect.DeepEqual(want, got) {
			t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
		}
	}
}
