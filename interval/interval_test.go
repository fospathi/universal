package interval

import (
	"reflect"
	"testing"
)

func TestIn_AbsLen(t *testing.T) {
	itl1 := In{-4, 4}
	itl2 := In{-8, -4}
	itl3 := In{-4, -8}

	want := 8.0
	got := itl1.AbsLen()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 4.0
	got = itl2.AbsLen()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = 4.0
	got = itl3.AbsLen()
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestIn_AsOpenIntersection(t *testing.T) {
	in1 := In{0, 1}
	in2 := In{0.5, 1.5}
	in3 := In{0.5, -0.5}
	in4 := In{-1, 2}
	in5 := In{0, 0.1}

	want1, want2 := In{0.5, 1.0}, true
	got1, got2 := in1.AsOpenIntersection(in2)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.5, 1.0}, true
	got1, got2 = in2.AsOpenIntersection(in1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{1.0, 0.5}, true
	got1, got2 = in2.Opposite().AsOpenIntersection(in1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.0, 0.5}, true
	got1, got2 = in1.AsOpenIntersection(in3)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.5, 0.0}, true
	got1, got2 = in3.AsOpenIntersection(in1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.0, 1.0}, true
	got1, got2 = in1.AsOpenIntersection(in4)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.0, 1.0}, true
	got1, got2 = in4.AsOpenIntersection(in1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{1.0, 0.0}, true
	got1, got2 = in4.Opposite().AsOpenIntersection(in1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.0, 1.0}, true
	got1, got2 = in1.AsOpenIntersection(in1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.0, 0.0}, false
	got1, got2 = in2.AsOpenIntersection(in3)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.0, 0.1}, true
	got1, got2 = in1.AsOpenIntersection(in5)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}

	want1, want2 = In{0.0, 0.1}, true
	got1, got2 = in5.AsOpenIntersection(in1)
	if want1 != got1 || want2 != got2 {
		t.Errorf("\nWant1:\n%#v\nGot1:\n%#v\nWant2:\n%#v\nGot2:\n%#v\n", want1, got1, want2, got2)
	}
}

func TestIn_ContractLimits(t *testing.T) {
	in1 := In{0, 1}
	in2 := In{0, -1}

	want := In{0.25, 0.75}
	got := in1.ContractLimits(0.25)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = In{-0.25, -0.75}
	got = in2.ContractLimits(0.25)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = In{-0.25, -0.75}
	got = in2.ContractLimits(-0.25)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = In{0.5, 0.5}
	got = in1.ContractLimits(0.5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = In{0.5, 0.5}
	got = in1.ContractLimits(0.55)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = In{-0.5, -0.5}
	got = in2.ContractLimits(0.5)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	want = In{-0.5, -0.5}
	got = in2.ContractLimits(0.55)
	if want != got {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}

func TestIn_Split(t *testing.T) {
	itl1 := In{0, 4}
	want := []In{{0, 1}, {1, 2}, {2, 3}, {3, 4}}
	got := itl1.Split(4)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	itl1 = In{0, 4}
	want = []In{{0, 2}, {2, 4}}
	got = itl1.Split(2)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	itl1 = In{4, 0}
	want = []In{{4, 0}}
	got = itl1.Split(1)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	itl1 = In{4, 0}
	want = []In{{4, 3}, {3, 2}, {2, 1}, {1, 0}}
	got = itl1.Split(4)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}

	itl1 = In{-1, -5}
	want = []In{{-1, -2}, {-2, -3}, {-3, -4}, {-4, -5}}
	got = itl1.Split(4)
	if !reflect.DeepEqual(want, got) {
		t.Errorf("\nWant:\n%#v\nGot:\n%#v\n", want, got)
	}
}
