/* Package pridi provides prettier ways of displaying some Go values. */
package pridi

import (
	"fmt"
	"strings"

	"gitlab.com/fospathi/universal/strlit"
	"golang.org/x/exp/slices"
)

// Struct value expressed as it might appear in a source code editor.
func Struct(v interface{}) string {
	var (
		es = &enclosureStack{}
		sb = strings.Builder{}
		// Map each string literal (key) to the slice indices (value) of its
		// occurrence(s) in the source code.
		literals = map[string][][2]int{}
		src      = fmt.Sprintf("%#v", v) // The value's Go source code.
		srcLen   = len(src)
		tab      = "    "
	)

	// Associate each string literal with its occurrences.
	for _, lit := range strlit.In(v) {
		literals[lit] = strlit.Occurrences(src, lit)
	}

	inAString := func(i int) bool {
		for _, spans := range literals {
			for _, span := range spans {
				if i >= span[0] && i < span[1] {
					return true
				}
			}
		}
		return false
	}

	nextIsA := func(i int, c byte) bool {
		if i+1 < srcLen && src[i+1] == c {
			return true
		}
		return false
	}

	prevIsA := func(i int, c byte) bool {
		if i-1 >= 0 && src[i-1] == c {
			return true
		}
		return false
	}

	for i := 0; i < srcLen; i++ {
		c := src[i]
		if inAString(i) {
			sb.WriteByte(c)
			continue
		}
		switch c {
		case '{':
			es.push(brace)
			sb.WriteByte(c)
			if !nextIsA(i, '}') {
				sb.WriteByte('\n')
				sb.WriteString(es.indentation(tab))
			}
		case '}':
			es.pop()
			if !prevIsA(i, '{') {
				sb.WriteByte('\n')
				sb.WriteString(es.indentation(tab))
			}
			sb.WriteByte(c)
		case '(':
			es.push(paren)
			sb.WriteByte(c)
		case ')':
			es.pop()
			sb.WriteByte(c)
		case ',':
			sb.WriteByte(c)
			if es.current() == brace {
				sb.WriteByte('\n')
				sb.WriteString(es.indentation(tab))
				if nextIsA(i, ' ') {
					i++ // Skip the space after a comma.
				}
			}
		case ':':
			sb.WriteByte(c)
			sb.WriteByte(' ')
		default:
			sb.WriteByte(c)
		}
	}

	return sb.String()
}

// The syntax used for delineation of the current delineation 'scope'.
type enclosure int

const (
	root  = enclosure(iota)
	brace = iota
	paren = iota
)

// A stack of nested enclosures.
type enclosureStack struct {
	ve []enclosure
}

func (es *enclosureStack) current() enclosure {
	if l := len(es.ve); l > 0 {
		return es.ve[l-1]
	}
	return root
}

func (es *enclosureStack) indentation(tab string) string {
	var n int
	for _, e := range es.ve {
		if e == brace {
			n++
		}
	}
	return strings.Repeat(tab, n)
}

func (es *enclosureStack) pop() {
	if l := len(es.ve); l > 0 {
		es.ve = slices.Delete(es.ve, l-1, l)
	}
}

func (es *enclosureStack) push(e enclosure) {
	es.ve = append(es.ve, e)
}
