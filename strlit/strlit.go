/*
Package strlit provides the ability to obtain the string literals present in a
Go value.
*/
package strlit

import (
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"strings"
)

// In the Go source code representation of the given Go value return the string
// literal constants.
//
// The returned strings are inclusive of the enclosing quotes of a string.
func In(v interface{}) []string {
	tree, _ := parser.ParseExpr(fmt.Sprintf("%#v", v))
	vis := visitor{}
	ast.Walk(&vis, tree)
	return vis.stringLiterals
}

type visitor struct {
	stringLiterals []string
}

func (v *visitor) Visit(n ast.Node) ast.Visitor {
	if n == nil {
		return nil
	}

	switch x := n.(type) {
	case *ast.BasicLit:
		if x.Kind == token.STRING {
			v.stringLiterals = append(v.stringLiterals, x.Value)
		}
	}
	return v
}

// Occurrences of the needle string, without overlaps, in the hay string.
//
// The occurrences are returned as pairs of Go slice compatible indices.
func Occurrences(hay, needle string) [][2]int {
	var (
		hayLen    = len(hay)
		needleLen = len(needle)
		spans     [][2]int
	)
	if needleLen == 0 {
		return nil
	}
	for i := 0; i < hayLen; i += needleLen {
		j := strings.Index(hay[i:], needle)
		if j == -1 {
			break
		}
		i += j
		spans = append(spans, [2]int{i, i + needleLen})
	}
	return spans
}
