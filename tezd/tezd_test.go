package tezd

import (
	"testing"
)

const (
	err string = "the floats should compare as equal at this precision"
)

func Test_CompareFloat64(t *testing.T) {
	f1 := 1.334
	f2 := 1.3340000007
	if !CompareFloat64(f1, f2, 5) {
		t.Errorf(err)
	}
	if CompareFloat64(f1, f2, 10) {
		t.Errorf("the floats should not compare as equal at this precision")
	}

	f1 = 1.99999999
	f2 = 2.0
	if !CompareFloat64(f1, f2, 5) {
		t.Errorf("the floats should compare as equal at this precision")
	}
	if CompareFloat64(f1, f2, 10) {
		t.Errorf("the floats should not compare as equal at this precision")
	}

	f1 = -0.000000001
	f2 = 0.0
	if !CompareFloat64(f1, f2, 5) {
		t.Errorf("the floats should compare as equal at this precision")
	}
	if CompareFloat64(f1, f2, 10) {
		t.Errorf("the floats should not compare as equal at this precision")
	}
}

func Test_CompareFloat64s(t *testing.T) {
	func() {
		f1 := []float64{}
		f2 := []float64{}
		if !CompareFloat64s(f1, f2, 5) {
			t.Errorf(err)
		}
	}()

	func() {
		f1 := [2]float64{1.0, 1.0}
		f2 := [1]float64{1.0}
		if CompareFloat64s(f1[:], f2[:], 5) {
			t.Errorf(err)
		}
	}()

	func() {
		f1 := [2]float64{1.0, -1.0}
		f2 := [2]float64{1.0, -0.99999999}
		if !CompareFloat64s(f1[:], f2[:], 5) {
			t.Errorf(err)
		}
	}()
}
