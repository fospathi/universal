/*
Package tezd provides comparison functions for use in unit tests.
*/
package tezd

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/fospathi/universal/interval"
)

// CompareBytes is whether whether two slices of byte have the same length and
// contain equal values.
func CompareBytes(vb1 []byte, vb2 []byte) bool {
	l := len(vb1)
	if l != len(vb2) {
		return false
	}
	for i := 0; i < l; i++ {
		if vb1[i] != vb2[i] {
			return false
		}
	}
	return true
}

// CompareFloat64 returns whether two floats are approximately equal. Each float
// is rounded to precision decimal places.
func CompareFloat64(f1 float64, f2 float64, precision int) bool {
	s1 := fmt.Sprintf("%.*f", precision, f1)
	s2 := fmt.Sprintf("%.*f", precision, f2)
	r1, _ := strconv.ParseFloat(s1, 64)
	r2, _ := strconv.ParseFloat(s2, 64)
	return r1 == r2
}

// CompareFloat64s returns whether two slices of float64 have the same length
// and contain approximately equal values. Each float is rounded to precision
// decimal places.
func CompareFloat64s(f1 []float64, f2 []float64, precision int) (equal bool) {
	l := len(f1)
	if l != len(f2) {
		return
	}
	for i := 0; i < l; i++ {
		if !CompareFloat64(f1[i], f2[i], precision) {
			return
		}
	}
	return true
}

// CompareInts returns whether two slices of int have the same length and
// contain equal values.
func CompareInts(vi1 []int, vi2 []int) bool {
	l := len(vi1)
	if l != len(vi2) {
		return false
	}
	for i := 0; i < l; i++ {
		if vi1[i] != vi2[i] {
			return false
		}
	}
	return true
}

// CompareIntervals returns whether two intervals have approximately overlapping
// domains. The directions are ignored.
func CompareIntervals(
	in1 interval.In,
	in2 interval.In,
	precision int,
) (equal bool) {

	min1, max1 := in1.MinMax()
	min2, max2 := in2.MinMax()
	return CompareFloat64(min1, min2, precision) &&
		CompareFloat64(max1, max2, precision)
}

// CompareStrings reports whether two slices of string have the same length and
// contain the same values in the same order.
func CompareStrings(vs1 []string, vs2 []string) bool {
	l := len(vs1)
	if l != len(vs2) {
		return false
	}
	for i := 0; i < l; i++ {
		if vs1[i] != vs2[i] {
			return false
		}
	}
	return true
}

// WaitMS starts a sleep for the argument number of milliseconds.
func WaitMS(ms int) {
	time.Sleep(time.Duration(ms) * time.Millisecond)
}
